//
//  DataManager.h
//  HuddleMe
//
//  Created by Approutes on 2/1/16.
//  Copyright © 2016 Approutes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkServices.h"
#import "JsonHandler.h"
#import "UrlsManager.h"
#import "SHBaseRequest.h"
#import "CMGolfCourseNameModel.h"


@protocol CallBackDelegate <NSObject>

@optional
-(void)didGetAllDataArray:(NSMutableArray *)inArray;

@end


@interface DataManager : NSObject<RequestDeligate>
{
//	NSMutableArray *parseArray;
	NSMutableArray *addressArray;
	//NSMutableArray *googlePlaceArray;
	NSMutableArray *huddleInfoArray;
	NSMutableArray *arrMyHuddleProfile;
	NSDictionary *huddleInfoDic;
	NSDictionary *locationDic;
	NSMutableArray *huddleAttendeesArray;
	NSMutableArray *huddleAttendeesNameArray;
	
}
@property (strong, nonatomic) NSDictionary *responseDict;
@property (strong, nonatomic)NSArray *jsonArrayfromResponse;
@property (strong, nonatomic)NSArray *arrayFromJsonArray;

@property(nonatomic,strong)NSMutableArray *sharedArrayMember;
@property(nonatomic,strong)NSMutableArray *sharedArrayPlayer;
@property(nonatomic,strong)NSMutableArray *sharedArrayMarker;
@property(nonatomic,strong)NSMutableArray *sharedArrayLadies;

@property(nonatomic,strong)NSMutableArray *parseArray;
@property(nonatomic,strong)NSMutableArray *parseArrayForHole;
@property(nonatomic,weak)id<CallBackDelegate>callBackDelegate;

+(DataManager *)dataManager;
-(NSDictionary *)fetchLocationInfo;
-(void)updateGlobalSettings;
-(void)fetchPlaceInfo:(id)inResponse;

@end
