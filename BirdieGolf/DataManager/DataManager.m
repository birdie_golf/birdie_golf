//
//  DataManager.m
//  HuddleMe
//
//  Created by Approutes on 2/1/16.
//  Copyright © 2016 Approutes. All rights reserved.
//

#import "DataManager.h"
#import "NetworkServices.h"



static DataManager *_sharedInstance ;

@implementation DataManager

+(DataManager *)dataManager{
	
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		_sharedInstance = [[DataManager alloc] init];
	});
	return  _sharedInstance;
}

-(instancetype)init{
	self = [super init];
	if (self) {
		//
//		[[DataManager dataManager].globalSettings setName:@"Shakir"];

	}
	return  self;
}
#pragma mark-
#pragma mark- Lazy Instantiation For Global Use

-(NSMutableArray*)sharedArrayMember {

	if (_sharedArrayMember==nil) {
		_sharedArrayMember=[[NSMutableArray alloc]init];
	}
	return _sharedArrayMember;
}
-(NSMutableArray*)sharedArrayPlayer {
	
	if (_sharedArrayPlayer==nil) {
		_sharedArrayPlayer=[[NSMutableArray alloc]init];
	}
	return _sharedArrayPlayer;
}
-(NSMutableArray*)sharedArrayMarker {
	
	if (_sharedArrayMarker==nil) {
		_sharedArrayMarker=[[NSMutableArray alloc]init];
	}
	return _sharedArrayMarker;
}
-(NSMutableArray*)sharedArrayLadies {
	
	if (_sharedArrayLadies==nil) {
		_sharedArrayLadies=[[NSMutableArray alloc]init];
	}
	return _sharedArrayLadies;
}



-(void)updateGlobalSettings{
	
//	NSData  *aSettingsData	=	[NSKeyedArchiver archivedDataWithRootObject: _globalSettings];
//	[[NSUserDefaults standardUserDefaults] setObject:aSettingsData forKey:@"CMGlobalSettings"];
//	[[NSUserDefaults standardUserDefaults] synchronize];
//	
}
-(void)fetchPlaceInfo:(id)inResponse
{
    _parseArray=[JsonHandler getGolfListDataFromJsonObject:inResponse];
    
//    _parseArrayForHole=[
    if ([DataManager dataManager].callBackDelegate  &&  [[DataManager dataManager].callBackDelegate conformsToProtocol:@protocol(CallBackDelegate)]  &&  [[DataManager dataManager].callBackDelegate respondsToSelector:@selector(didGetAllDataArray:)] ) {
        [[DataManager dataManager].callBackDelegate didGetAllDataArray:_parseArray];
    }
}





-(NSDictionary *)fetchLocationInfo
{
    locationDic = [[NSDictionary alloc]init]; //dictionaryWithObjectsAndKeys:@"1imageIcon",@"locationImage",@"The cafe in the Corner",@"locationName",@"29 Bridge Street,Sydney",@"locationAddress", nil];
	return locationDic;
	
}



- (void)responseHandler :(id)inResponseDic andRequestIdentifier :(NSString*)inReqIdentifier
{
	
	if ([inReqIdentifier isEqualToString:kGetGolfList]) {
		[self fetchPlaceInfo:inResponseDic];
      
    }
	if ([inReqIdentifier isEqualToString:kGetPaidGolfCourseDetail]) {
		NSLog(@"kGetPaidGolfCourseDetail");
		
	}

	
}
- (void)requestErrorHandler :(NSError *)inError andRequestIdentifier :(NSString *)inReqIdentifier
{
	NSLog(@"error");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!!!"
                                                    message:@"Cannot connect to network"
                                                   delegate:self
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
	
}
@end
