//
//  CMGolfCourseHolesModel.h
//  BirdieGolf
//
//  Created by AppRoutes on 23/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CMGolfCourseHolesModel : NSObject

@property (nonatomic,strong) NSString* holeId;
@property (nonatomic,strong) NSString* holeVideoUrl;
@property (nonatomic,strong) NSMutableArray* holeUrlArray;
- (id)initWithResponse:(id)response;

@end
