//
//  CMGolfCourseHolesModel.m
//  BirdieGolf
//
//  Created by AppRoutes on 23/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import "CMGolfCourseHolesModel.h"
#import "UrlsManager.h"
@implementation CMGolfCourseHolesModel
- (id)initWithResponse:(id)response
{
    self = [super init];
    
    if (self) {
        
        if (response && [response count]>0) {
            [self parseData:response];
        }
    }
    return self;
}
-(void)parseData:(id)responseObject
{

    if ([responseObject objectForKey:kHoleId]) {
        self.holeId = [NSString stringWithFormat:@"%@",[responseObject objectForKey:kHoleId]];
    }
    if ([responseObject objectForKey:kHoleVideoUrl]) {
        self.holeVideoUrl = [NSString stringWithFormat:@"%@",[responseObject objectForKey:kHoleVideoUrl]];
    }
}
@end
