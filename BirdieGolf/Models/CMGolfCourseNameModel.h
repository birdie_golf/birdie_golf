//
//  CMGolfCourseNameModel.h
//  BirdieGolf
//
//  Created by AppRoutes on 23/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import"HomeScreenViewController.h"
#import "CMGolfCourseHolesModel.h"

@interface CMGolfCourseNameModel : NSObject



@property (nonatomic,strong) NSString* closeTime;
@property (nonatomic,strong) NSString* distance;
@property (nonatomic,strong) NSString* golfAddress;
@property (nonatomic,strong) NSString* golfId;
@property (nonatomic,strong) NSString* golfName;
@property (nonatomic,strong) NSString* golfState;
@property (nonatomic,strong) NSString* holeUrl;

@property (nonatomic,strong) NSMutableArray* holeId;
@property (nonatomic,strong) NSMutableArray* holeVideoUrl;

@property (nonatomic,strong) NSString *latitude;
@property (nonatomic,strong) NSString *longitude;
@property (nonatomic,strong) NSString* numOfHole;
@property (nonatomic,strong) NSString* openTime;
@property (nonatomic,strong) NSString* staticMap;

@property (nonatomic,strong) NSString* status;
@property (nonatomic,strong) NSString* radius;
@property (nonatomic,strong) NSString* minRadius;
@property (nonatomic,strong) NSString* message;
@property (nonatomic,strong) NSString* serverUrl;

//@property (nonatomic,strong) NSMutableArray *dataDitc;
@property (nonatomic,strong) NSMutableArray* holeUrlArray;

@property (nonatomic,assign) BOOL isLocked;

- (id)initWithResponse:(id)response;
@end
