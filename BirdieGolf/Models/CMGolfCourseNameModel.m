
//
//  CMGolfCourseNameModel.m
//  BirdieGolf
//
//  Created by AppRoutes on 23/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import "CMGolfCourseNameModel.h"
#import "UrlsManager.h"
@implementation CMGolfCourseNameModel
//@synthesize golfName,golfAddress,imageUrl,latitude,longitude,isLocked,golfNameArray;
//-(instancetype)init{
//    
//    self = [super init];
//    
//    if (self) {
//        
//        //initialize code here.
//        
//    }
////    NSLog(@"golfName %@",golfName);
//    return  self;
//    
//}
- (id)initWithResponse:(id)response
{
    self = [super init];
    
    if (self) {
        
    if (response && [response count]>0) {
        [self parseData:response];
    }
    }
    return self;
}
-(void)parseData:(id)responseObject
{
    if ([responseObject objectForKey:kCloseTime]) {
        self.closeTime = [NSString stringWithFormat:@"%@",[responseObject objectForKey:kCloseTime]];
    }
    if ([responseObject objectForKey:kDistance]) {
        self.distance = [NSString stringWithFormat:@"%@",[responseObject objectForKey:kDistance]];
    }
    if ([responseObject objectForKey:kGolfAddress]) {
        self.golfAddress = [NSString stringWithFormat:@"%@",[responseObject objectForKey:kGolfAddress]];
    }
    if ([responseObject objectForKey:kGolfId]) {
        self.golfId = [NSString stringWithFormat:@"%@",[responseObject objectForKey:kGolfId]];
    }
    if ([responseObject objectForKey:kGolfName]) {
        self.golfName = [NSString stringWithFormat:@"%@",[responseObject objectForKey:kGolfName]];
    }
    if ([responseObject objectForKey:kGolfState]) {
        self.golfState = [NSString stringWithFormat:@"%@",[responseObject objectForKey:kGolfState]];
    }
    if ([responseObject objectForKey:kHoleUrl]) {
        
        NSArray *allDataDict=(NSArray *)[responseObject objectForKey:kHoleUrl];
         _holeId=[[NSMutableArray alloc]init];
        self.holeVideoUrl=[[NSMutableArray alloc]init];
        if ([allDataDict count]>0) {
            [allDataDict enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                CMGolfCourseHolesModel *aHoleModel = [[CMGolfCourseHolesModel alloc]initWithResponse:obj];
//                aHoleModel.holeId = [NSString stringWithFormat:@"%@",[obj objectForKey:kHoleId]];
//                aHoleModel.holeVideoUrl = [NSString stringWithFormat:@"%@",[obj objectForKey:kHoleVideoUrl]];
//                [self.holeUrlArray addObject:aHoleModel];
               
                if ([obj objectForKey:kHoleId]) {
                    
                    NSString *holeID=[NSString stringWithFormat:@"%@",[obj objectForKey:kHoleId]];
                    [_holeId addObject:holeID];
                }
                if ([obj objectForKey:kHoleVideoUrl]) {
                    [self.holeVideoUrl addObject:[NSString stringWithFormat:@"%@",[obj objectForKey:kHoleVideoUrl]]];
                }
                }];
        }
    }

    if ([responseObject objectForKey:kLatitude]) {
        self.latitude = [NSString stringWithFormat:@"%@",[responseObject objectForKey:kLatitude]];
    }
    if ([responseObject objectForKey:kLongitude]) {
        self.longitude = [NSString stringWithFormat:@"%@",[responseObject objectForKey:kLongitude]];
    }
    if ([responseObject objectForKey:kNumOfHole]) {
        self.numOfHole = [NSString stringWithFormat:@"%@",[responseObject objectForKey:kNumOfHole]];
    }
    if ([responseObject objectForKey:kStaticMap]) {
        self.staticMap = [NSString stringWithFormat:@"%@",[responseObject objectForKey:kStaticMap]];
    }
}
//Lazy Instantiation 
//- (NSMutableArray *)golfNameArray
//{
//    if (_holeUrlArray == nil) {
//        _holeUrlArray = [[NSMutableArray alloc] init];
//    }
//    return _holeUrlArray;
//}


@end
