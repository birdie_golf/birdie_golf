//
//  CMLadiesVC.h
//  BirdieGolf
//
//  Created by Approutes on 11/05/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CMLadiesVC : NSObject
@property (strong, nonatomic) NSString *strTxtMeters;
@property (strong, nonatomic) NSString *strTxtPar;
@property (strong, nonatomic) NSString *strTxtIndex;

- (id)initWithResponse:(id)response;
@end
