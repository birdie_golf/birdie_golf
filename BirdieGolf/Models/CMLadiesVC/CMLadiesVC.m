//
//  CMLadiesVC.m
//  BirdieGolf
//
//  Created by Approutes on 11/05/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import "CMLadiesVC.h"
#import "LadiesViewTableCell.h"
@implementation CMLadiesVC
- (id)initWithResponse:(id)response
{
	self = [super init];
	
	if (self) {
		
		if (response)// && [response count]>0)
		{
			[self textFieldData:response];
		}
	}
	return self;
}
-(void)textFieldData:(id)responseObject
{
	LadiesViewTableCell *cell=responseObject;
	_strTxtMeters=cell.txtMeters.text;
	_strTxtPar=cell.txtPar.text;
	_strTxtIndex=cell.txtIndex.text;
	NSLog(@"_strLblGolfHoles : %@ _strTxtPar : %@ _strTxtIndex : %@",_strTxtMeters,_strTxtPar,_strTxtIndex);
	
}

@end
