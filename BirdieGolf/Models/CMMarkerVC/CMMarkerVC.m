//
//  CMMarkerVC.m
//  BirdieGolf
//
//  Created by Approutes on 11/05/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import "CMMarkerVC.h"
#import "MarkerViewTableCell.h"
@implementation CMMarkerVC
- (id)initWithResponse:(id)response
{
	self = [super init];
	
	if (self) {
		
		if (response)// && [response count]>0)
		{
			[self textFieldData:response];
		}
	}
	return self;
}
-(void)textFieldData:(id)responseObject
{
	MarkerViewTableCell *cell=responseObject;
	_noOfHolesStr=cell.noOfHolesLbl.text;
	_txt_2_FieldStr=cell.txtField2Hole.text;
	_txt_0_FieldStr=cell.txtFieldZeroHole.text;
	
	NSLog(@"_strLblGolfHoles :%@ _strTxtMeters :%@ _strTxtPar :%@",_noOfHolesStr,_txt_2_FieldStr,_txt_0_FieldStr);
	
}

@end
