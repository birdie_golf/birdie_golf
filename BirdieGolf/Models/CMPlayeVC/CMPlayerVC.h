//
//  CMPlayerVC.h
//  BirdieGolf
//
//  Created by Approutes on 11/05/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CMPlayerVC : NSObject
@property (strong, nonatomic) NSString *noOfHolesStr;
@property (strong, nonatomic) NSString *txt_1_FieldStr;
@property (strong, nonatomic) NSString *txt_2_FieldStr;
@property (strong, nonatomic) NSString *txt_0_FieldStr;

- (id)initWithResponse:(id)response;
@end
