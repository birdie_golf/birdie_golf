//
//  CMPlayerVC.m
//  BirdieGolf
//
//  Created by Approutes on 11/05/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import "CMPlayerVC.h"
#import "PlayerViewTableCell.h"
@implementation CMPlayerVC
- (id)initWithResponse:(id)response
{
	self = [super init];
	
	if (self) {
		
		if (response)// && [response count]>0)
		{
			[self textFieldData:response];
		}
	}
	return self;
}
-(void)textFieldData:(id)responseObject
{
	PlayerViewTableCell *cell=responseObject;
	_noOfHolesStr=cell.noOfHoles.text;
	_txt_2_FieldStr=cell.txt_2_Field.text;
	_txt_0_FieldStr=cell.txt_0_Field.text;
	
	NSLog(@"_strLblGolfHoles :%@ _strTxtMeters :%@ _strTxtPar :%@",_noOfHolesStr,_txt_2_FieldStr,_txt_0_FieldStr);
	
}

@end
