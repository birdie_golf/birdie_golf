//
//  MyCustomAnnotation.h
//  BirdieGolf
//
//  Created by Approutes on 02/05/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface MyCustomAnnotation : NSObject<MKAnnotation>
@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy)		NSString* title;
-(id)initWithTitle:(NSString*)inTitle andLocation:(CLLocationCoordinate2D)inLocation;
-(MKAnnotationView*)annotationView;
@end
