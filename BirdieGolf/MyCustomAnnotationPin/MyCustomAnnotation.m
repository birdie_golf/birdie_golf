//
//  MyCustomAnnotation.m
//  BirdieGolf
//
//  Created by Approutes on 02/05/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import "MyCustomAnnotation.h"

@implementation MyCustomAnnotation
-(id)initWithTitle:(NSString*)inTitle andLocation:(CLLocationCoordinate2D)inLocation
{
	self = [super init];
	if (self) {
		_title=inTitle;
		_coordinate=inLocation;
	}
	return self;
}
-(MKAnnotationView*)annotationView
{
	MKAnnotationView *annotationView=[[MKAnnotationView alloc]initWithAnnotation:self reuseIdentifier:@"MyCustomAnnotation"];
	annotationView.enabled=YES;
	annotationView.canShowCallout=YES;
	annotationView.image=[UIImage imageNamed:@"gulfCourseIcon.png"];
	return annotationView;
}

@end
