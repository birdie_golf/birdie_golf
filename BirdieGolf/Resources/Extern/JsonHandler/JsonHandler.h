//
//  JsonHandler.h
//  BirdieGolf
//
//  Created by Approutes on 21/04/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkServices.h"
#import "NetworkServiceConstant.h"
#import "DataManager.h"
#import "SHBaseRequest.h"
#import "CMGolfCourseNameModel.h"

@interface JsonHandler : NSObject

//{
//    id<TableDataDelegate>tableCellDelegate;
//}

@property (nonatomic,strong) NSMutableArray *   golfNameArray;
@property (nonatomic,strong) NSMutableArray *   golfAddressArray;
@property (nonatomic,strong) NSMutableArray *   GolfNameAndHoleUrlArray;
@property (nonatomic,strong) NSDictionary   *   dictGolfNameAndHoleUrl;

+(JsonHandler *)jsonHandler;
+(NSMutableArray *)getGolfListDataFromJsonObject:(NSDictionary *)inResponse;


@end
