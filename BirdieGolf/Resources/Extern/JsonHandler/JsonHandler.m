//
//  JsonHandler.m
//  BirdieGolf
//
//  Created by Approutes on 21/04/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import "JsonHandler.h"
#import "UrlsManager.h"
#import "HomeScreenViewController.h"
#import "MainCollectionViewController.h"
#import "WelcomeScreenViewController.h"
#import "JsonRequest.h"

#import "UIImageView+WebCache.h"

static JsonHandler *_sharedInstance;
@implementation JsonHandler

+(JsonHandler *)jsonHandler
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedInstance = [[JsonHandler alloc] init];
    });
    return  _sharedInstance;

}
+(NSMutableArray *)getGolfListDataFromJsonObject:(NSDictionary *)inResponse;
{
    NSMutableArray* golfDetailsArray=[[NSMutableArray alloc]init];
    CMGolfCourseNameModel *cmGCNModel=[[CMGolfCourseNameModel alloc]init];
    
#pragma Marker - Status
    NSString *statusData=(NSString *)[inResponse objectForKey:kStatus];
    if (statusData==(id)[NSNull null]) {
         NSLog(@"String is nil");
    }
    else
    {
        cmGCNModel.status=statusData;
    }
    
#pragma Marker - Radius
    NSString *radiusData=(NSString *)[inResponse objectForKey:kRadius];
    if (radiusData==(id)[NSNull null]) {
        NSLog(@"Radius is nil");
    }
    else
    {
        cmGCNModel.radius=radiusData;
    }

#pragma Marker - MinRadius
    NSString *minRadiusData=(NSString *)[inResponse objectForKey:kMinRadius];
    if (minRadiusData==(id)[NSNull null]) {
        NSLog(@"MinRadius is nil");
    }
    else
    {
        cmGCNModel.minRadius=minRadiusData;
    }

#pragma Marker - Message
    NSString *messageData=(NSString *)[inResponse objectForKey:kMessage];
    if (messageData==(id)[NSNull null]) {
        NSLog(@"Message is nil");
    }
    else
    {
        cmGCNModel.message=messageData;
    }

#pragma Marker - ServerUrl
    NSString *serverUrlData=(NSString *)[inResponse objectForKey:kServerUrl];
    if (serverUrlData==(id)[NSNull null]) {
        NSLog(@"ServerUrl is nil");
    }
    else
    {
        cmGCNModel.serverUrl=serverUrlData;
		[[NSUserDefaults standardUserDefaults] setObject:serverUrlData forKey:kServerUrl];
    }
    
#pragma Marker - AllData
    NSArray *allDataDict=(NSArray *)[inResponse objectForKey:kAllData];
    if (allDataDict == (id)[NSNull null])
    {
        NSLog(@"array is nil");
    }
    else
    {
        if ([allDataDict count]>0) {
            [allDataDict enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                CMGolfCourseNameModel *aCMGolfCourseNameModel=[[CMGolfCourseNameModel alloc]initWithResponse:obj];
                [golfDetailsArray addObject:aCMGolfCourseNameModel];
            }];
        }
        
    }
    return golfDetailsArray;
}


@end
