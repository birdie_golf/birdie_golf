//
//  JsonRequest.h
//  BirdieGolf
//
//  Created by Approutes on 23/04/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkServices.h"
#import "NetworkServiceConstant.h"
#import "DataManager.h"
#import "SHBaseRequest.h"
#import "UrlsManager.h"

@interface JsonRequest : NSObject
+(void)initNetworkServicesWithLatitude:(NSString *)inLatitude AndLongitude:(NSString *)inLongitude;
+(void)requestForPaidGolfCourseDetailGolfID:(NSString*)inGolfId;
@end
