//
//  JsonRequest.m
//  BirdieGolf
//
//  Created by Approutes on 23/04/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import "JsonRequest.h"

@implementation JsonRequest

+(void)initNetworkServicesWithLatitude:(NSString *)inLatitude AndLongitude:(NSString *)inLongitude
{
    NSMutableDictionary *aDic=[[NSMutableDictionary alloc]init];
    [aDic setObject:kGetGolfList forKey:kOption];
    [aDic setObject:inLatitude forKey:kLat];
    [aDic setObject:inLongitude forKey:kLong];
	[[NSUserDefaults standardUserDefaults]setObject:inLatitude forKey:kLatitude];
	[[NSUserDefaults standardUserDefaults]setObject:inLongitude forKey:kLatitude];
 [[NetworkServices sharedInstance]hitWebServiceForUrl:[UrlsManager getLocationUrl] withData:aDic requestType:eReqPost reqContentType:eReqContentAppJson forKey:kGetGolfList handlerDelegate:[DataManager dataManager]];
    
}

+(void)requestForPaidGolfCourseDetailGolfID:(NSString*)inGolfId
{
	NSMutableDictionary *aDic=[[NSMutableDictionary alloc]init];
	[aDic setObject:kGetPaidGolfCourseDetail forKey:kOption];
	[aDic setObject:inGolfId forKey:kGolfId];
	[aDic setObject:[[NSUserDefaults standardUserDefaults]valueForKey:kUniqueId] forKey:kUniqueId];
	[aDic setObject:[[NSUserDefaults standardUserDefaults]valueForKey:kLatitude] forKey:kLatitude];
	[aDic setObject:[[NSUserDefaults standardUserDefaults]valueForKey:kLatitude] forKey:kLongitude];
	
	[[NetworkServices sharedInstance]hitWebServiceForUrl:[UrlsManager getLocationUrl] withData:aDic requestType:eReqPost reqContentType:eReqContentAppJson forKey:kGetPaidGolfCourseDetail handlerDelegate:[DataManager dataManager]];

}

@end
