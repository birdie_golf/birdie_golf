//
//  NetworkServiceConstant.h
//  ByteClub
//
//  Created by Mac on 11/25/15.
//  Copyright © 2015 Razeware. All rights reserved.
//

#ifndef NetworkServiceConstant_h
#define NetworkServiceConstant_h

#define kReqPOST  @"POST"
#define kReqGET  @"GET"
#define kReqTimeout  60.0


#define kReqContentApplicationJson  @"application/JSON"
#define kReqContentAppFormEncoded  @"application/x-www-form-urlencoded"
#define kReqContentPlainText  @"text/plain"

#endif /* NetworkServiceConstant_h */
