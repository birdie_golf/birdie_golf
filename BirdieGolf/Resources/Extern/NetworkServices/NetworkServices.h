//
//  NetworkServices.h
//  ByteClub
//
//  Created by Mac on 11/25/15.
//  Copyright © 2015 Razeware. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NetworkServiceConstant.h"
@import UIKit;// for UI components......

@protocol imageDownloadedDelegate<NSObject>
@optional
-(void) getImageDownloadDelegateMethod:(UIImage*)downloadedImage;

@end


@protocol RequestDeligate;
typedef NS_ENUM(NSInteger,enReqContentType)
{
	eReqContentAppJson = 1,
	eReqContentAppXWWWForm,
	eReqContentTextPlain
};

typedef NS_ENUM(NSInteger,enReqType)
{
	eReqPost = 1,
	eReqGet
};

@interface NetworkServices : NSObject
@property (nonatomic,weak) id<imageDownloadedDelegate> imageDelegate;

+(NetworkServices *)sharedInstance;

-(void)hitWebServiceForUrl:(NSString *)inUrl withData:(id)inDict requestType:(enReqType)inReqType reqContentType:(enReqContentType)inReqContentType forKey:(NSString *)inKey handlerDelegate:(id<RequestDeligate>)inDelegate;

-(void)downloadImageFromUrl:(NSString*)imgUrl;
@end

extern NetworkServices *gNetworkServices;