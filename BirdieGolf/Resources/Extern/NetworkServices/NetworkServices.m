 //
//  NetworkServices.m
//  ByteClub
//
//  Created by Mac on 11/25/15.
//  Copyright © 2015 Razeware. All rights reserved.
//

#import "NetworkServices.h"
#import "SHBaseRequest.h"
NetworkServices *gNetworkServices = nil;

@interface NetworkServices ()<NSURLSessionDataDelegate>{
	
}
@property(nonatomic , strong) NSMutableDictionary *processingRequests;//we can handle request for api key.
@property(nonatomic , strong) NSMutableArray *processingRequestKeys;


@end
@implementation NetworkServices


+(NetworkServices *)sharedInstance{
	
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		gNetworkServices = [[NetworkServices alloc] init];
	});
	return  gNetworkServices;
}
-(instancetype)init{
	
	self = [super init];
	if (self) {
		
		//Initialize code here.
		
	}
	return self;
}

-(NSMutableDictionary *)processingRequests{

	if (_processingRequests == nil) {
		_processingRequests = [[NSMutableDictionary alloc] init];
	}
	return _processingRequests;
}
-(NSMutableArray *)processingRequestKeys{
	
	if (_processingRequestKeys == nil) {
		_processingRequestKeys = [[NSMutableArray alloc] init];
	}
	return _processingRequestKeys;

}
-(void)hitWebServiceForUrl:(NSString *)inUrl withData:(id)inDict requestType:(enReqType)inReqType reqContentType:(enReqContentType)inReqContentType forKey:(NSString *)inKey handlerDelegate:(id<RequestDeligate>)inDelegate{
	NSString*	aReqContentType = nil;
	NSString*	aReqType = nil;

	if ([self.processingRequestKeys containsObject : inKey] == YES) {
		NSLog(@"request already is executing ");
		return;
	}
	switch (inReqContentType)
	{
		case eReqContentAppJson:
			aReqContentType = kReqContentApplicationJson;
			break;
			
		case eReqContentAppXWWWForm:
			aReqContentType = kReqContentAppFormEncoded;
			break;
			
		case eReqContentTextPlain:
			aReqContentType = kReqContentPlainText;
			break;
			
		default:
			break;
	}
	
	switch (inReqType)
	{
		case eReqGet:
			aReqType = kReqGET;
			break;
			
		case eReqPost:
			aReqType = kReqPOST;
			break;
			
		default:
			break;
	}
	

	NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];

	NSURLSession *aSession = [NSURLSession sessionWithConfiguration: config delegate: self delegateQueue:[NSOperationQueue mainQueue]];

	inUrl = [inUrl stringByReplacingOccurrencesOfString:@" " withString:@"%20"];

	__block SHBaseRequest *aRequest = [[SHBaseRequest alloc] initWithURL: [NSURL URLWithString: inUrl] cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval: kReqTimeout];
	
	[aRequest setHTTPMethod:aReqType];
	aRequest.ResponseClassDelegate = inDelegate;
	aRequest.RequestIdentifier = inKey;
	
	NSData *jsonData = [NSJSONSerialization dataWithJSONObject:inDict options:NSJSONWritingPrettyPrinted error:nil];
	NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
	
	
	
	[aRequest addValue:aReqContentType forHTTPHeaderField:@"Content-Type"];
	[aRequest setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];

	
	
	__block NSURLSessionDataTask *aSessionTask = [aSession dataTaskWithRequest:aRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
		
        id arrResult;
        if(data!=nil)
        {
		 arrResult  =[NSJSONSerialization JSONObjectWithData: data
													   options: NSJSONReadingMutableContainers
						
                                                         error: &error];
          NSString* responseString = [[NSString alloc]initWithData:data encoding:NSASCIIStringEncoding];
            NSLog(@"%@",responseString);

        }
        else
        {
            NSLog(@"Problem No data Recieved from server!!");
        }
		if (arrResult) {
			
			if (aRequest.ResponseClassDelegate && [aRequest.ResponseClassDelegate conformsToProtocol:@protocol(RequestDeligate)] && [aRequest.ResponseClassDelegate respondsToSelector:@selector(responseHandler:andRequestIdentifier:)]) {
    [aRequest.ResponseClassDelegate responseHandler: arrResult andRequestIdentifier: aRequest.RequestIdentifier];
			}
		}
		else{
			if (aRequest.ResponseClassDelegate && [aRequest.ResponseClassDelegate conformsToProtocol:@protocol(RequestDeligate)] && [aRequest.ResponseClassDelegate respondsToSelector:@selector(requestErrorHandler:andRequestIdentifier:)]) {
    [aRequest.ResponseClassDelegate requestErrorHandler:error andRequestIdentifier:aRequest.RequestIdentifier];

			}
		}
		NSLog(@"current request  == %@",aRequest.RequestIdentifier);

		
	}];
	[aSessionTask resume];
	[self.processingRequests setObject: @"ee" forKey: inKey];

}


- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
didReceiveResponse:(NSURLResponse *)response
 completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler{
	
}
-(void)downloadImageFromUrl:(NSString*)imgUrl
{
    //imgUrl = @"http://www.gettyimages.ca/gi-resources/images/Homepage/Category-Creative/UK/UK_Creative_462809583.jpg";
    NSURLSessionConfiguration *sessionConfig =[NSURLSessionConfiguration defaultSessionConfiguration];
    
    NSURLSession *session =[NSURLSession sessionWithConfiguration:sessionConfig
                                  delegate:self
                             delegateQueue:nil];

    
 //  __block UIImage* image;
    NSURLSessionDownloadTask *getImageTask =
    [session downloadTaskWithURL:[NSURL URLWithString:imgUrl]
     
               completionHandler:^(NSURL *location,
                                   NSURLResponse *response,
                                   NSError *error) {

                   UIImage *downloadedImage =
                   [UIImage imageWithData:
                    [NSData dataWithContentsOfURL:location]];

                                         if(downloadedImage)
                       {
                           // call back delegate after download completion of image...................
//                           if (self.imageDelegate && [self.imageDelegate conformsToProtocol: @protocol(imageDownloadedDelegate)] && [self.imageDelegate respondsToSelector:@selector(getImageDownloadDelegateMethod:)]) {
//                               [self.imageDelegate getImageDownloadDelegateMethod:downloadedImage];
                        //   imageView.image=downloadedImage;
                       //    }
                           NSLog(@"Image url");
                       }
                   NSLog(@"%@",error);
                   
               }];
    
    
    [getImageTask resume];
    
}
@end
