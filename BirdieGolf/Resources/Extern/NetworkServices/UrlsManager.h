//
//  UrlsManager.h
//  Insurance App
//
//  Created by Mac on 8/14/15.
//  Copyright (c) 2015 Kunal Khanna. All rights reserved.
//

#import <Foundation/Foundation.h>

#define	kBaseUrl                                        @"http://devapiwipo.approutes.com:80/"
#define KImageBaseUrl                                   @"https://s3-eu-west-1.amazonaws.com/devciaoim.com/"
#define kLogin                                          @"login"
#define kLat                                            @"lat"
#define kLong                                           @"long"
#define kGetGolfList                                    @"getGolfList"
#define kOption                                         @"option"

#define kStatus                                         @"status"
#define kRadius                                         @"radius"

#define kAllData                                        @"allData"
#define kCloseTime                                      @"closeTime"
#define kDistance                                       @"distance"
#define kGolfAddress                                    @"golfAddress"
#define kGolfId                                         @"golfId"
#define kGolfName                                       @"golfName"
#define kGolfState                                      @"golfState"
#define kHoleUrl                                        @"holeUrl"
#define kHoleId                                         @"holeId"
#define kHoleVideoUrl                                   @"holeVideoUrl"
#define kLatitude                                       @"latitude"
#define kLongitude                                      @"longitude"
#define kNumOfHole                                      @"numOfHole"
#define kOpenTime                                       @"openTime"
#define kStaticMap                                      @"staticMap"

#define kMinRadius                                      @"minRadius"
#define kMessage                                        @"message"
#define kServerUrl                                      @"serverUrl"

#define kDeviceId										@"deviceId"
#define kGetPaidGolfCourseDetail						@"getPaidGolfCourseDetail"
#define kUniqueId										@"uniqueId"
@interface UrlsManager : NSObject


+(NSString *)getLocationUrl;

@end
