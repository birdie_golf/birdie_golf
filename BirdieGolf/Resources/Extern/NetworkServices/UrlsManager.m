//
//  UrlsManager.m
//  Insurance App
//
//  Created by Mac on 8/14/15.
//  Copyright (c) 2015 Kunal Khanna. All rights reserved.
//

#import "UrlsManager.h"


@implementation UrlsManager

+(NSString *)getBaseUrl{
	return kBaseUrl;
}


+(NSString*)getLocationUrl
{
	NSString *aUrlStr = nil;
	
	aUrlStr = [NSString stringWithFormat:@"%@%@",[UrlsManager getBaseUrl],kLogin];
	
	return aUrlStr;
	
}

@end
