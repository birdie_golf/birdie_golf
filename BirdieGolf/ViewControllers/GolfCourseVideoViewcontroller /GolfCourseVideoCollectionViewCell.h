//
//  GolfCourseVideoCollectionViewCell.h
//  BirdieGolf
//
//  Created by Arpit Shukla on 25/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GolfCourseVideoViewController.h"
#import "DataManager.h"

@interface GolfCourseVideoCollectionViewCell : UICollectionViewCell 
@property (strong, nonatomic) IBOutlet UILabel *lblvideo;
@property(strong,nonatomic)NSIndexPath *indexPath;

- (void)updateCellData:(NSMutableArray *)golfArray;

@end
