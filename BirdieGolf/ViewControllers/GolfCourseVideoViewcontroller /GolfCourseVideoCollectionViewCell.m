//
//  GolfCourseVideoCollectionViewCell.m
//  BirdieGolf
//
//  Created by Arpit Shukla on 25/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import "GolfCourseVideoCollectionViewCell.h"
#import "CMGolfCourseNameModel.h"

@implementation GolfCourseVideoCollectionViewCell

- (void)updateCellData:(NSMutableArray *)nameArray
{
    _lblvideo.text = [nameArray objectAtIndex:_indexPath.row];
}
@end
