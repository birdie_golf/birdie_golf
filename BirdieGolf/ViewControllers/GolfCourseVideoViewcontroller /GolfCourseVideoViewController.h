//
//  GolfCourseVideoViewController.h
//  BirdieGolf
//
//  Created by Arpit Shukla on 25/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GolfCourseVideoCollectionViewCell.h"
@interface GolfCourseVideoViewController : UICollectionViewController
@property(strong,nonatomic)NSMutableArray* dataArray;
@end
