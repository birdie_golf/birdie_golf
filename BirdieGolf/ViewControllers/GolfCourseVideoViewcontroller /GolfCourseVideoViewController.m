//
//  GolfCourseVideoViewController.m
//  BirdieGolf
//
//  Created by Arpit Shukla on 25/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import "GolfCourseVideoViewController.h"
#import "GolfCourseVideoCollectionViewCell.h"

@interface GolfCourseVideoViewController ()

@end

@implementation GolfCourseVideoViewController
@synthesize dataArray;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    dataArray=[[NSMutableArray alloc]init];
    dataArray = [NSMutableArray arrayWithObjects: @"Royal Sydney Golf Club",@"More Park Golf",@"The Lake Golf  Course", nil];
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.dataArray.count;
}
- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    GolfCourseVideoCollectionViewCell* cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionCell" forIndexPath:indexPath];
    cell.indexPath = indexPath;
    [cell updateCellData:dataArray ];
    return cell;
}
@end
