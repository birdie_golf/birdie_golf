//
//  HomeCollectionViewCell.h
//  BirdieGolf
//
//  Created by Arpit Shukla on 27/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *lblGolfCourse;
- (void)updateCell:(NSString*)strLabel;

@end
