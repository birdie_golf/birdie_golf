//
//  HomeScreenTableViewCell.h
//  BirdieGolf
//
//  Created by Arpit Shukla on 24/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "homeScreenViewController.h"
#import "DataManager.h"

#import "UIImageView+WebCache.h"
#import "UrlsManager.h"

@interface HomeScreenTableViewCell : UITableViewCell
{
	MKPinAnnotationView *customPinView;

}
@property (strong, nonatomic) NSIndexPath *customPinView;

@property (strong, nonatomic) NSIndexPath *indexPath;
@property (strong, nonatomic) IBOutlet	UILabel	*lblGolfCourseName;
@property (strong, nonatomic) IBOutlet	UILabel	*lblGolfCourseAddress;
@property (strong, nonatomic) IBOutlet	UIImageView *imgLocked;
@property (strong, nonatomic) IBOutlet	UIImageView	*staticMapImgV;


- (void)updateCellData:(CMGolfCourseNameModel *)aGolfCourseName;

@end

