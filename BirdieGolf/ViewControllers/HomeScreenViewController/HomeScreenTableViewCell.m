//
//  HomeScreenTableViewCell.m
//  BirdieGolf
//
//  Created by Arpit Shukla on 24/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import "HomeScreenTableViewCell.h"
#import "CMGolfCourseNameModel.h"

@implementation HomeScreenTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)updateCellData:(CMGolfCourseNameModel *)aGolfCourseName
{
    _lblGolfCourseName.text = aGolfCourseName.golfName;
    _lblGolfCourseAddress.text=aGolfCourseName.golfAddress;
    if (aGolfCourseName.isLocked) {
        _imgLocked.image = [UIImage imageNamed:@"loclkIconActive.png"];
    }
    else
    {
        _imgLocked.image = [UIImage imageNamed:@"loclkIconActive.png"];
    }
    [_staticMapImgV sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",KImageBaseUrl,aGolfCourseName.staticMap]] placeholderImage:[UIImage imageNamed:@""] options:SDWebImageRetryFailed completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
    }];
//	NSString *stringURL = [NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] valueForKey:kServerUrl],aGolfCourseName.staticMap];
//	NSURL  *url = [NSURL URLWithString:stringURL];
//	NSData *urlData = [NSData dataWithContentsOfURL:url];
//	UIImage *newImage = [UIImage imageWithData:urlData];
//	//_staticMapImgV.image=newImage;
//	UIImageView *imageView = [[UIImageView alloc] initWithImage:newImage];
//	imageView.frame = CGRectMake(0,0,30,30);
//
//
	_staticMapImgV.layer.cornerRadius=6.0f;
	_staticMapImgV.clipsToBounds=YES;
}


@end
