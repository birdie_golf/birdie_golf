//
//  homeScreenViewController.h
//  Birdie Golf
//
//  Created by AppRoutes on 20/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.


#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "HomeCollectionViewCell.h"
#import <CoreLocation/CoreLocation.h>

#import "CMGolfCourseHolesModel.h"
#import "JsonRequest.h"
#import "CMGolfCourseNameModel.h"
#import "JsonHandler.h"
#import "MyCustomAnnotation.h"
#import "UIImageView+WebCache.h"


@interface HomeScreenViewController : UIViewController <MKMapViewDelegate, CLLocationManagerDelegate, MKOverlay,UISearchBarDelegate,UISearchControllerDelegate,CLLocationManagerDelegate,
	   UITableViewDataSource,UITableViewDelegate>
{
@private
	CGRect _searchTableViewRect;
}
@property (assign, nonatomic) BOOL                                              isEnable;
@property (assign, nonatomic) BOOL                                              isDisable;
@property (assign, nonatomic) BOOL                                              isSelectedRow;
@property (strong, nonatomic) IBOutlet  MKMapView               *               mapView;
@property (strong, nonatomic) IBOutlet  UICollectionView        *               homecollectionView;
@property (strong, nonatomic) IBOutlet  UITableView             *               tblHomeScreenController;
@property (strong, nonatomic) IBOutlet UIImageView *collectionViewImage;

@property (strong, nonatomic)           UIButton                *               someButton;
@property (strong, nonatomic)           UIBarButtonItem         *               listButton;
@property (strong, nonatomic)           UIImage                 *               image3;
@property (strong, nonatomic)           CMGolfCourseHolesModel  *               aHoleModel;
@property (strong, nonatomic)			NSMutableArray			*				allDataArray;
@property (strong, nonatomic) IBOutlet	UIActivityIndicatorView	*				activityIndicator;
@property (strong, nonatomic) IBOutlet	UIView					*				activityindicatorView;
@property (nonatomic)					CLLocationCoordinate2D                  CoordinateData;
@property(strong,nonatomic)UIActivityIndicatorView *indicatorView;
@property(retain,nonatomic)NSTimer *activityTimer;

@property (strong, nonatomic)NSString *showHideSelection;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *tblViewTopCons;


// Search
//@property (strong, nonatomic) UISearchController *searchController;
@property (strong, nonatomic) MKLocalSearch *localSearch;
@property (strong, nonatomic) MKLocalSearchResponse *results;
- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view;

	

@end
