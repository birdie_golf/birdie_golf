//
//  homeScreenViewController.m
//  Birdie Golf
//
//  Created by AppRoutes on 20/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import "homeScreenViewController.h"
#import <MapKit/MapKit.h>
#import "HomeScreenTableViewCell.h"
#import "CMGolfCourseNameModel.h"

#import "JsonHandler.h"
#import "JsonRequest.h"
#import "MyCustomAnnotation.h"
#import "MainCollectionViewController.h"
#import "DataManager.h"
#import "WelcomeScreenViewController.h"


@interface HomeScreenViewController ()<CLLocationManagerDelegate, CallBackDelegate>

{
    NSMutableData *data;
	NSInteger	showHide;
	
}
- (void) setupLocationManager;
- (BOOL) enableLocationServices;


- (void) searchQuery:(NSString *)query;

@end

@implementation HomeScreenViewController
{
    CLLocationManager *locationManager;
    CLGeocoder *geocoder;
    MKPlacemark *placemark;
}

@synthesize mapView,someButton,listButton,image3,CoordinateData;
@synthesize tblHomeScreenController;
@synthesize homecollectionView;
//@synthesize searchController;
@synthesize localSearch;
@synthesize results;
//@synthesize locationManager;


#define METERS_PER_MILE 1609.344
//#define kBaseUrl @"http://devapiwipo.approutes.com:80/"

- (void)showHide{
	showHide=[_showHideSelection integerValue];
	switch (showHide) {
  case 1:
			self.searchDisplayController.searchBar.hidden=false;
			
			break;
  case 2:
			self.searchDisplayController.searchBar.hidden=true;
			
			break;
			
  default:
			break;
	}
}

- (void)viewDidLoad {
    [super viewDidLoad];
	    [homecollectionView setHidden:YES];
		[_collectionViewImage setHidden:YES];
		_activityindicatorView.alpha=0.0;
		tblHomeScreenController.hidden=_isEnable;
		homecollectionView.hidden=_isDisable;
		[DataManager dataManager].callBackDelegate=self;
		locationManager = [[CLLocationManager alloc]init];
		geocoder = [[CLGeocoder alloc] init];
		mapView.delegate=self;
		mapView.showsUserLocation=YES;
		locationManager.delegate=self;
		locationManager.desiredAccuracy=kCLLocationAccuracyThreeKilometers;
	if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8)
	{
		[locationManager  requestWhenInUseAuthorization];
	}
		[locationManager startUpdatingLocation];
	
	// Keep the subviews inside the top and bottom layout guides
	self.edgesForExtendedLayout = UIRectEdgeLeft | UIRectEdgeBottom | UIRectEdgeRight;
	// Fix black glow on navigation bar
	[self.navigationController.view setBackgroundColor:[UIColor whiteColor]];
	
	// Set up location operators
//	[self setupLocationManager];
	// Set up search operators
	
	
	
	// Should make search bar extend underneath status bar (DOES NOT WORK)
	self.definesPresentationContext = YES;
	[self customNavigationController];
	  }

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	[self showHide];
	_tblViewTopCons.constant=0;
	[tblHomeScreenController layoutIfNeeded];
	tblHomeScreenController.hidden=_isEnable;
	homecollectionView.hidden=_isDisable;
	_isSelectedRow=NO;
	[self customRightBarBtnImage];
}



- (void)didReceiveMemoryWarning
{
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}
//search
- (BOOL) enableLocationServices {
	
	if ([CLLocationManager locationServicesEnabled]) {
		locationManager.distanceFilter = 10;
		locationManager.desiredAccuracy = kCLLocationAccuracyBest;
		[locationManager startUpdatingLocation];
		[self.mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
		return YES;
	} else {
		return NO;
	}
}
- (void) setupLocationManager {
	locationManager = [[CLLocationManager alloc] init];
	locationManager.delegate = self;
	
	// Will call locationManager:didChangeAuthorizationStatus: delegate method
	[CLLocationManager authorizationStatus];
}
- (void) locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
	
	NSString *message = @"You must enable Location Services for this app in order to use it.";
	NSString *button = @"Ok";
	NSString *title;
	
	if (status == kCLAuthorizationStatusDenied) {
		title = @"Location Services Disabled";
		[[[UIAlertView alloc] initWithTitle:title
							   message:message
							  delegate:self
						  cancelButtonTitle:nil
						  otherButtonTitles:button, nil] show];
	} else if(status == kCLAuthorizationStatusRestricted) {
		title = @"Location Services Restricted";
		[[[UIAlertView alloc] initWithTitle:title
							   message:message
							  delegate:self
						  cancelButtonTitle:nil
						  otherButtonTitles:button, nil] show];
	} else if (status == kCLAuthorizationStatusAuthorizedWhenInUse) {
		// Note: kCLAuthorizationStatusAuthorizedWhenInUse depends on the request...Authorization
		// (Always or WhenInUse)
		if ([self enableLocationServices]) {
			NSLog(@"Location Services enabled.");
		} else {
			NSLog(@"Couldn't enable Location Services. Please enable them in Settings > Privacy > Location Services.");
		}
	} else if (status == kCLAuthorizationStatusNotDetermined) {
		NSLog(@"Error : Authorization status not determined.");
		[locationManager requestWhenInUseAuthorization];
	}
}
- (void)searchQuery:(NSString *)query {
	// Cancel any previous searches.
	[self.localSearch cancel];
	
	MKLocalSearchRequest *request = [[MKLocalSearchRequest alloc] init];
	request.naturalLanguageQuery = query;
	request.region = self.mapView.region;
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	self.localSearch = [[MKLocalSearch alloc] initWithRequest:request];
	
	[self.localSearch startWithCompletionHandler:^(MKLocalSearchResponse *response, NSError *error){
		
		[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
		
		if (error != nil) {
			[[[UIAlertView alloc] initWithTitle:@"Map Error"
								   message:[error description]
								  delegate:nil
							  cancelButtonTitle:@"OK"
							  otherButtonTitles:nil] show];
			return;
		}
		
		//			if ([response.mapItems count] == 0) {
		//				[[[UIAlertView alloc] initWithTitle:@"No Results"
		//											message:nil
		//										   delegate:nil
		//								  cancelButtonTitle:@"OK"
		//								  otherButtonTitles:nil] show];
		//				return;
		//			}
		
		self.results = response;
		
		[self.searchDisplayController.searchResultsTableView  reloadData];
	}];
}


- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
	
	if (![searchText isEqualToString:@""]) {
		[self searchQuery:searchText];
	}
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)aSearchBar {
	[self searchQuery:aSearchBar.text];
}


//- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//	
//	static NSString *IDENTIFIER = @"SearchResultsCell";
//	
//	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:IDENTIFIER];
//	if (cell == nil) {
//		cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:IDENTIFIER];
//	}
//	
//	MKMapItem *item = self.results.mapItems[indexPath.row];
//	
//	cell.textLabel.text = item.placemark.name;
//	cell.detailTextLabel.text = item.placemark.addressDictionary[@"Street"];
//	
//	return cell;
//}

//- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
//	
//	// Hide search controller
//	[self.searchController setActive:NO];
//	
//	MKMapItem *item = self.results.mapItems[indexPath.row];
//	
//	NSLog(@"Selected \"%@\"", item.placemark.name);
//	
//	[self.mapView addAnnotation:item.placemark];
//	[self.mapView selectAnnotation:item.placemark animated:YES];
//	
//	[self.mapView setCenterCoordinate:item.placemark.location.coordinate animated:YES];
//	
//	[self.mapView setUserTrackingMode:MKUserTrackingModeNone];
//	
//}

#pragma mark - Navigation Bar
- (void)customNavigationController
{
	self.navigationItem.title=@"Home";
	self.navigationItem.hidesBackButton=NO;
	[self customLeftBarBtnImage];
	[self customRightBarBtnImage];                                                                      //Custom Bar Button
}
- (void)customLeftBarBtnImage
{
	UIButton *backBtn =[UIButton buttonWithType:UIButtonTypeCustom];
	UIImage *imageBackBtn=[UIImage imageNamed:@"backIconWhite"];
	[backBtn setFrame:CGRectMake(0.0f,0.0f,imageBackBtn.size.width, imageBackBtn.size.height)];
	[backBtn setBackgroundImage:imageBackBtn forState:UIControlStateNormal];
	[self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:backBtn]];
	[backBtn addTarget:self action:@selector(popToRootVC) forControlEvents:UIControlEventTouchUpInside];
}

- (void)popToRootVC
{
	[self.navigationController popToRootViewControllerAnimated:YES];
}
- (void)customRightBarBtnImage
{
	if (tblHomeScreenController.hidden==YES)
	{
		image3 = [UIImage imageNamed:@"listView.png"];
	}
	else
	{
		image3 = [UIImage imageNamed:@"location22.png"];
	}
	CGRect frameimg = CGRectMake(11,11, image3.size.width, image3.size.height);
	self.someButton = [[UIButton alloc] initWithFrame:frameimg];
	[self.someButton setBackgroundImage:image3 forState:UIControlStateNormal];
	
	[someButton addTarget:self action:@selector(golfListAndMapSelector) forControlEvents:UIControlEventTouchUpInside];
	listButton =[[UIBarButtonItem alloc]initWithCustomView:someButton];
	self.navigationItem.rightBarButtonItem=listButton;
}
#pragma mark- customize activity indicator
//[self showActivityindicator];
// _indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//
////	_indicatorView.frame = CGRectMake(150,200, 20, 20);
////	[_indicatorView.layer setBackgroundColor:[[UIColor colorWithWhite: 1.0 alpha:1.0] CGColor]];
////
////	[self.view addSubview:_indicatorView];
////
////	[_indicatorView startAnimating];
////
////self.activityTimer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(stopActivity:) userInfo:nil repeats:NO];
//}
//-(void) stopActivity:(NSTimer *) theTimer
//{
//	[_indicatorView stopAnimating];
//	[_activityTimer invalidate];
//	_indicatorView = nil;
//}


#pragma mark - activityIndicator
- (void)showActivityindicator
{
	[UIView animateWithDuration:10.0 animations:^{
		_activityindicatorView.alpha=0.8;
	} completion:^(BOOL finished) {
[_activityIndicator startAnimating];		
	}];
}
- (void)hideActivityindicator
{
	[UIView animateWithDuration:3.0 animations:^{
		_activityindicatorView.alpha=0.0;
	} completion:^(BOOL finished) {
		[tblHomeScreenController reloadData];
		_tblViewTopCons.constant=0;
	 [tblHomeScreenController layoutIfNeeded];
		[_activityIndicator stopAnimating];
		NSArray *points=[self getAllGolfServicePoints:_allDataArray];
		[self.mapView addAnnotations:points];

	}];
}
#pragma mark - delegation method
- (void)didGetAllDataArray:(NSMutableArray *)inArray;
{
	[self showActivityindicator];
	_allDataArray=[[NSMutableArray alloc]initWithArray:inArray];
	[self hideActivityindicator];
	
}

- (void)golfListAndMapSelector
{
    if (tblHomeScreenController.hidden==NO)
	{
	 
        [self golfCourseMap];
    }
    else
        [self golfCourseListView];
}

#pragma mark - update location
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
	CLLocation *currentLocation = newLocation;
	CLLocationCoordinate2D coordinateIn2D;
	if (currentLocation != nil) 
	{
		NSString *strLongitude= [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.longitude];
		NSString *strLatitude = [NSString stringWithFormat:@"%.8f", currentLocation.coordinate.latitude];
		NSLog(@"New Location %@ %@",strLatitude,strLongitude);
		
		//Create coordinates from the latitude and longitude values
		coordinateIn2D.latitude=currentLocation.coordinate.latitude;
		coordinateIn2D.longitude=currentLocation.coordinate.longitude;
		[JsonRequest initNetworkServicesWithLatitude:strLatitude AndLongitude:strLongitude]; //Requesting for Data From Server
	}
	[locationManager stopUpdatingLocation];
	//CLLocationDistance fenceDistance = 300;
	//CLLocationCoordinate2D circleMiddlePoint = CLLocationCoordinate2DMake(coordinateIn2D.latitude, coordinateIn2D.longitude);
	//MKCircle *circle = [MKCircle circleWithCenterCoordinate:circleMiddlePoint radius:fenceDistance];
	//[mapView addOverlay: circle];
	

}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	NSLog(@"didFailWithError: %@", error);
	UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Title" message:@"Failed to Get Your Location" preferredStyle:UIAlertControllerStyleAlert];
	
	UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
	[alertController addAction:ok];
	[self presentViewController:alertController animated:YES completion:nil];
}


#pragma mark - viewForAnnotation
- (MKAnnotationView *)mapView:(MKMapView *)theMapView viewForAnnotation:(id <MKAnnotation>)annotation
{
	__block UIImageView *aCustomImageView;
	NSString *identifier=@"MyCustomAnnotation";
	__block MKAnnotationView *customPinView;
	customPinView=nil;
	customPinView=(MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
        if (customPinView==nil) {
            customPinView = [[MKAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:identifier];
        }
        else
        {
            customPinView.annotation=annotation;
        }
	
		NSString *strv = annotation.title;
		customPinView.canShowCallout=YES;
	
	if ([customPinView.annotation.title isEqualToString:@"Current Location"])
		{
			
			customPinView.image=[UIImage imageNamed:@"location.png"];
			UIImage *img=[UIImage imageNamed:@"profileIcon"];
			aCustomImageView = [[UIImageView alloc] initWithImage:img];
			[aCustomImageView setFrame:CGRectMake(0, 0, img.size.width-5, img.size.height-5)];
			customPinView.leftCalloutAccessoryView=aCustomImageView;
			customPinView.rightCalloutAccessoryView=nil;
	
			MKCircle *circle = [MKCircle circleWithCenterCoordinate:mapView.userLocation.coordinate radius:5000];
			[self zoomToLocation:mapView.userLocation.coordinate];

			[mapView addOverlay:circle];
		}
	else
		{
//			leftCalloutAccessoryView

			customPinView.image=[UIImage imageNamed:@"gulfCourseIcon.png"];
			
//		 for(int i=0;i<_allDataArray.count;i++)
//			{
//			 CMGolfCourseNameModel *golfCourses=[_allDataArray objectAtIndex:i];
//				if([strv isEqualToString:golfCourses.golfName] )
//				    {
//					NSString *stringURL = [NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] valueForKey:kServerUrl],golfCourses.staticMap];
//					NSURL  *url = [NSURL URLWithString:stringURL];
//					NSData *urlData = [NSData dataWithContentsOfURL:url];
//					//UIImage *newImage = [UIImage imageWithData:urlData];
//					
//					
//					//aCustomImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"profileIcon"]];
//					
//					//	customPinView.leftCalloutAccessoryView=nil;
//					aCustomImageView = [[UIImageView alloc] init];
//
//					
//					//aCustomImageView.image=newImage;
//					customPinView.leftCalloutAccessoryView=[[UIImageView alloc]initWithImage:[UIImage imageWithData:urlData scale:3]];
//					break;
//				    }					//return ;
//
//			}
			dispatch_queue_t queue = dispatch_queue_create("ThreadTask",NULL);
			dispatch_queue_t main = dispatch_get_main_queue();
			dispatch_async(queue,
						   ^{
							   //do the fetching of data here(Don't do any UI Updates)
							   NSData *urlData;
							   		 for(int i=0;i<_allDataArray.count;i++)
							   			{
							   			 CMGolfCourseNameModel *golfCourses=[_allDataArray objectAtIndex:i];
							   				if([strv isEqualToString:golfCourses.golfName] )
							   				    {
							   					NSString *stringURL = [NSString stringWithFormat:@"%@%@",[[NSUserDefaults standardUserDefaults] valueForKey:kServerUrl],golfCourses.staticMap];
							   					NSURL  *url = [NSURL URLWithString:stringURL];
							   					urlData = [NSData dataWithContentsOfURL:url];
							   					break;
							   				    }
							   			}

							   dispatch_async(main,
											  ^{
												  // Do the UI Update here.
											aCustomImageView = [[UIImageView alloc] init];
											customPinView.leftCalloutAccessoryView=[[UIImageView alloc]initWithImage:[UIImage imageWithData:urlData scale:3]];
											  });
						   });

//			rightCalloutAccessoryView
		UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
			UIImage *imageRightButton = [UIImage imageNamed:@""];
			rightButton.frame = CGRectMake(0, 0, imageRightButton.size.width, imageRightButton.size.height);
			[rightButton setImage:imageRightButton forState:UIControlStateNormal];
//			[rightButton addTarget:self action:@selector(didTapCalloutButton) forControlEvents:UIControlEventTouchUpInside];
			customPinView.rightCalloutAccessoryView = rightButton;
		}
	
    return customPinView;
}

-(void)didTapCalloutButton
{
//	MainCollectionViewController *mainCollectionVc = [self.storyboard instantiateViewControllerWithIdentifier:@"MainCollectionViewController"];
//	[self.navigationController pushViewController:mainCollectionVc animated:YES];
}


- (MKOverlayRenderer *) mapView:(MKMapView *)mapView rendererForOverlay:(id)overlay
{
	if ([overlay isKindOfClass:[MKCircle class]])
	    {
		MKCircleRenderer* aRenderer = [[MKCircleRenderer alloc] initWithCircle:(MKCircle *)overlay];
		
		aRenderer.fillColor = [[UIColor cyanColor] colorWithAlphaComponent:0.2];
		aRenderer.strokeColor = [[UIColor blueColor] colorWithAlphaComponent:0.4];
		aRenderer.lineWidth = 3;
		return aRenderer;
	    }else{
		    return nil;
	    }
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
	NSLog(@"callout annotation.title = %@", view.annotation.title);
	
	MainCollectionViewController *mainCollectionVc = [self.storyboard instantiateViewControllerWithIdentifier:@"MainCollectionViewController"];
	[_allDataArray enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
		if ([view.annotation.title isEqualToString:[obj valueForKey:kGolfName]]) {
			mainCollectionVc.aGolfCourseName=obj;
			dispatch_async(dispatch_get_main_queue(), ^{
				[JsonRequest requestForPaidGolfCourseDetailGolfID:mainCollectionVc.aGolfCourseName.golfId];
				
			});
			

		}
	}];
	[self.navigationController pushViewController:mainCollectionVc animated:YES];

}
- (NSMutableArray *)getAllGolfServicePoints:(NSMutableArray *)golfcourseModels
{
	NSMutableArray *points = [[NSMutableArray alloc] init];
	
	for (CMGolfCourseNameModel *aGolfCourseNameModel in golfcourseModels)
	{
		MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
		CLLocationCoordinate2D pinlocation;
		pinlocation.latitude = aGolfCourseNameModel.latitude.doubleValue;
		pinlocation.longitude = aGolfCourseNameModel.longitude.doubleValue;
		point.coordinate = pinlocation;
		point.title = aGolfCourseNameModel.golfName;
		point.subtitle = aGolfCourseNameModel.golfAddress;
		[points addObject:point];
	 
	}
	return points;
}

#pragma mark -zoomToLocation
- (void)zoomToLocation:(CLLocationCoordinate2D)inCoordinate
{
    CLLocationCoordinate2D zoomLocation;
    zoomLocation.latitude=inCoordinate.latitude;
    zoomLocation.longitude=inCoordinate.longitude;
    MKCoordinateRegion viewRegion = MKCoordinateRegionMakeWithDistance(zoomLocation,16*METERS_PER_MILE,16*METERS_PER_MILE);
    [self.mapView setRegion:viewRegion animated:YES];
    [self.mapView regionThatFits:viewRegion];
}

- (void)golfCourseListView
{
	self.searchDisplayController.searchBar.hidden=true;
	[tblHomeScreenController setFrame:CGRectMake(0, 0, 320, 504)];
	tblHomeScreenController.tableHeaderView =nil;
    if (tblHomeScreenController.hidden==NO)
	{
        tblHomeScreenController.hidden=YES;
        homecollectionView.hidden=NO;
//        [self viewDidLoad];
    }
	else
	{
        tblHomeScreenController.hidden=NO;
        image3 = [UIImage imageNamed:@"locationIconWhite.png"];
        homecollectionView.hidden=YES;
        [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
        [self.tblHomeScreenController reloadData]; //just call reloadData on the tableView
    }
}
- (void)golfCourseMap
{
	self.searchDisplayController.searchBar.hidden=false;
    if (tblHomeScreenController.hidden==YES)
	{
        tblHomeScreenController.hidden=NO;
        homecollectionView.hidden=YES;
//        [self viewDidLoad];
    }
	else
	{
        tblHomeScreenController.hidden=YES;
        image3 = [UIImage imageNamed:@"listViewIcon.png"];
        homecollectionView.hidden=NO;
        [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
//        [self.tblHomeScreenController reloadData]; //just call reloadData on the tableView
    }
}


#pragma mark-collection cell
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 9;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
   NSString *SimpleIdentifier = @"HomeCollectionCell";
		HomeCollectionViewCell  *cellCollection=[collectionView dequeueReusableCellWithReuseIdentifier:SimpleIdentifier forIndexPath:indexPath];
//		cell.layer.borderWidth=1.0f;
		[cellCollection updateCell:@"Royal Sydney Club"];
    
    return cellCollection;
}


#pragma mark- table view implementation
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (tableView==tblHomeScreenController) {
		return  [_allDataArray count];
	}
	else
	return [self.results.mapItems count];

}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	if (tableView==tblHomeScreenController) {
		return 75;
	}
	else
	return 44;
}
- (UITableViewCell *) tableView:(UITableView * )tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *SimpleIdentifier = @"SimpleIdentifiers";
	
	static NSString *IDENTIFIER = @"SearchResultsCell";

	if (tableView == tblHomeScreenController) {
		HomeScreenTableViewCell* homeCell = [tableView dequeueReusableCellWithIdentifier:SimpleIdentifier forIndexPath:indexPath];
		if (homeCell == nil) {
			homeCell = [[HomeScreenTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:SimpleIdentifier];
		}
		CMGolfCourseNameModel *aGolfCourseName =[_allDataArray objectAtIndex:indexPath.row];
		[homeCell updateCellData:aGolfCourseName];
		homeCell.textLabel.font=[UIFont fontWithName:@"Arial Rounded MT Bold" size:15.0];
		homeCell.accessoryType=UITableViewCellAccessoryNone;
		return homeCell;
	}
	else
	{
		UITableViewCell *mapCell = [tableView dequeueReusableCellWithIdentifier:IDENTIFIER];
		if (mapCell == nil) {
			mapCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:IDENTIFIER];
		}
		MKMapItem *item = self.results.mapItems[indexPath.row];
		mapCell.textLabel.text = item.placemark.name;
		mapCell.detailTextLabel.text = item.placemark.addressDictionary[@"Street"];
		return mapCell;
	}
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	
	if (tableView == tblHomeScreenController) {
		if (!_isSelectedRow) {
			MainCollectionViewController *mainCollectionVc = [self.storyboard instantiateViewControllerWithIdentifier:@"MainCollectionViewController"];
			mainCollectionVc.aGolfCourseName =[_allDataArray objectAtIndex:indexPath.row];
			dispatch_async(dispatch_get_main_queue(), ^{
				[JsonRequest requestForPaidGolfCourseDetailGolfID:mainCollectionVc.aGolfCourseName.golfId];
				
			});
			[self.navigationController pushViewController:mainCollectionVc animated:YES];
			_isSelectedRow=YES;
		}
	}
	else {
		[self.searchDisplayController setActive:NO];
		
		MKMapItem *item = self.results.mapItems[indexPath.row];
		
		NSLog(@"Selected \"%@\"", item.placemark.name);
		
		[self.mapView addAnnotation:item.placemark];
		[self.mapView selectAnnotation:item.placemark animated:YES];
		
		[self.mapView setCenterCoordinate:item.placemark.location.coordinate animated:YES];
		
		[self.mapView setUserTrackingMode:MKUserTrackingModeNone];
	
	
		
}
}


- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
	[self searchQuery:searchText];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
	[self filterContentForSearchText:searchString
						  scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
							    objectAtIndex:[self.searchDisplayController.searchBar
										    selectedScopeButtonIndex]]];
	
	return YES;
}
@end


