//
//  ladiesViewController.m
//  BirdieGolf
//
//  Created by AppRoutes on 22/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import "LadiesViewController.h"
#import "LadiesViewTableCell.h"
#import "CMLadiesVC.h"

@interface LadiesViewController ()
{
    NSMutableArray *inOutTotal;
//    NSMutableArray *noOfHoles;
//        NSInteger length;
	
	NSMutableArray *arrayCMLadies;
}
@end

@implementation LadiesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

//    inOutTotal=[NSMutableArray arrayWithObjects:@"In",@"Out",@"Total", nil];
//    noOfHoles=[NSMutableArray arrayWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9", nil];
	[self NoOfRowInThirdSectionOfTableView];
	arrayCMLadies=[[NSMutableArray alloc]init];
	

	[self.view endEditing:true];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	if (!_isAppear) {
	[_ladiesTableView reloadData];	
	}
	
}

- (void)NoOfRowInThirdSectionOfTableView
{
	if ([_noOfHoles count]>9) {
		inOutTotal=[NSMutableArray arrayWithObjects:@"In",@"Out",@"Total", nil];
	}
	else
	{
		inOutTotal=[NSMutableArray arrayWithObjects:@"Total", nil];
	}
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cell_HeaderLadies=@"Cell_HeaderLadies";
    static NSString *cell_NoOfHoles = @"Cell_NoOfHoles";
    static NSString *cell_InOutTotal=@"Cell_InOutTotal";
    LadiesViewTableCell *startCell;
	
    switch (indexPath.section) {
        case 0:
            startCell= [tableView dequeueReusableCellWithIdentifier:cell_HeaderLadies forIndexPath:indexPath];
            if(startCell==nil){
                startCell =[[LadiesViewTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_HeaderLadies];
            }
            return startCell;
            break;
        case 1:
            startCell= [tableView dequeueReusableCellWithIdentifier:cell_NoOfHoles forIndexPath:indexPath];
            
            if(startCell==nil){
                startCell =[[LadiesViewTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_NoOfHoles];
            }
			
            [startCell updateCellData:[_noOfHoles objectAtIndex:indexPath.row]];
            
            return startCell;
            break;
        default:
            startCell= [tableView dequeueReusableCellWithIdentifier:cell_InOutTotal forIndexPath:indexPath];
            if(startCell==nil){
                startCell =[[LadiesViewTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_InOutTotal];
            }
            [startCell updateInOutTotalCell:[inOutTotal objectAtIndex:indexPath.row]];
            return startCell;
            break;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if(section==0)
        return 1;
    else if(section==1)
        return [_noOfHoles count];
    else
        return inOutTotal.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 3;
}

- (IBAction)tapGestureAction:(id)sender {
	[self.view endEditing:true];
}


@end
