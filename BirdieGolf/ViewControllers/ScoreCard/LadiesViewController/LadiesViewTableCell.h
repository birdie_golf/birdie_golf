//
//  LadiesViewTableCell.h
//  BirdieGolf
//
//  Created by Approutes on 18/04/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LadiesViewTableCell : UITableViewCell
{
	int count;
}
@property (strong, nonatomic) IBOutlet UITextField *txtMeters;
@property (strong, nonatomic) IBOutlet UITextField *txtPar;
@property (strong, nonatomic) IBOutlet UITextField *txtIndex;
//@property (strong, nonatomic) IBOutlet UITextField *txtIOTHoles;
@property (strong, nonatomic) IBOutlet UITextField *txtIOTPar;
@property (strong, nonatomic) IBOutlet UITextField *txtIOTIndex;
@property (strong, nonatomic) IBOutlet UILabel *iOTotalLbl;



- (void)updateCellData:(id)inGolfHole;

- (void)updateInOutTotalCell:(id)inIOTotal;
@end
