//
//  LadiesViewTableCell.m
//  BirdieGolf
//
//  Created by Approutes on 18/04/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import "LadiesViewTableCell.h"
#import "CMLadiesVC.h"
#import "DataManager.h"

@interface LadiesViewTableCell()<UITextFieldDelegate>
{
	CMLadiesVC *aCMLadies;
	
}
@end

@implementation LadiesViewTableCell

- (void)awakeFromNib {
//    [super awakeFromNib];
    // Initialization code
		[[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(didChangedTextField:) name:UITextFieldTextDidEndEditingNotification object: nil];
	_txtMeters.delegate = self;
	_txtPar.delegate=self;
	_txtIndex.delegate = self;

	count=0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateCellData:(id)inGolfHole
{
	aCMLadies=inGolfHole;
	_txtMeters.text=aCMLadies.strTxtMeters;
	_txtPar.text=aCMLadies.strTxtPar;
	_txtIndex.text=aCMLadies.strTxtIndex;
}
- (void)updateInOutTotalCell:(id)inIOTotal
{
    _iOTotalLbl.text=inIOTotal;
    _iOTotalLbl.textColor=[UIColor colorWithRed:13.0/255.0 green:85.0/255.0 blue:136.0/255.0 alpha:1];
	
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	
//	_txtMeters.delegate = self;
//	_txtPar.delegate=self;
//	_txtIndex.delegate = self;

	
	return self;
}

-(void)didChangedTextField:(NSNotification *)inNotification{
	if ([[inNotification name] isEqualToString: UITextFieldTextDidEndEditingNotification]) {
//		if (count%3==0) {
//			
//			aCMLadies=[[CMLadiesVC alloc]init];
//		}
		UITextField *aText = inNotification.object;
		if ([aText isEqual: _txtMeters]) {
			aCMLadies.strTxtMeters=aText.text;
		}
		if ([aText isEqual: _txtPar]) {
			aCMLadies.strTxtPar=aText.text;
		}
		if ([aText isEqual: _txtIndex]) {
			aCMLadies.strTxtIndex=aText.text;
		}
	}
	
//	++count;
//	
//	if (count%3==0) {
//		
//		[self fillSharedArray:aCMLadies];
//	}
	
}

- (void)fillSharedArray:(CMLadiesVC*)inCMLadies
{
	if ([inCMLadies.strTxtMeters length]>0 && [inCMLadies.strTxtPar length]>0 && [inCMLadies.strTxtIndex length]>0)
	{
		[[DataManager dataManager].sharedArrayLadies addObject:aCMLadies];
	}
	else
	{
//		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!!!"
//														message:@"Please Enter Valid Data"
//													   delegate:self
//											  cancelButtonTitle:@"OK"
//											  otherButtonTitles:nil];
//		[alert show];
	}
}
@end
