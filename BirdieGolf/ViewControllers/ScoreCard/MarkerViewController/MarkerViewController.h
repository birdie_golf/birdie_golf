//
//  markerViewController.h
//  BirdieGolf
//
//  Created by AppRoutes on 22/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarkerViewController : UIViewController <UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITableView *markerTableView;
@property (strong, nonatomic) NSString *noOfHolesInGolfCourse;
@property (retain, nonatomic) NSMutableArray *noOfHoles;
@property (nonatomic)		  NSInteger length;
@property (assign, nonatomic) BOOL isAppear;
- (IBAction)tapGestureAction:(id)sender;

@end
