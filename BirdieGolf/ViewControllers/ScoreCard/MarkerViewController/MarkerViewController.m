//
//  markerViewController.m
//  BirdieGolf
//
//  Created by AppRoutes on 22/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import "MarkerViewController.h"
#import "MarkerViewTableCell.h"
#import "CMMarkerVC.h"

@interface MarkerViewController ()
{
    NSMutableArray *inOutTotal;
//    NSMutableArray *noOfHoles;
//        NSInteger length;
	NSMutableArray *cells_Array;
	NSMutableArray *arrayCMMarker;
}

@end

@implementation MarkerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    inOutTotal=[NSMutableArray arrayWithObjects:@"In",@"Out",@"Total", nil];
	[self NoOfRowInThirdSectionOfTableView];
	cells_Array=[[NSMutableArray alloc]init];
	arrayCMMarker=[[NSMutableArray alloc]init];
	

//    noOfHoles=[NSMutableArray arrayWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9", nil];

}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	if (!_isAppear) {
		[_markerTableView reloadData];
		
	}
}

- (void)NoOfRowInThirdSectionOfTableView
{
	if ([_noOfHoles count]>9) {
		inOutTotal=[NSMutableArray arrayWithObjects:@"In",@"Out",@"Total", nil];
	}
	else
	{
		inOutTotal=[NSMutableArray arrayWithObjects:@"Total", nil];
	}
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *headerPlayerCell=@"headerPlayerCell";
    static NSString *cell_NoOfHoles = @"Cell_NoOfHoles";
    static NSString *cell_IOTotal=@"Cell_IOTotal";
    MarkerViewTableCell *startCell;
    startCell.indexPathData=indexPath;
    switch (indexPath.section) {
        case 0:
            startCell= [tableView dequeueReusableCellWithIdentifier:headerPlayerCell forIndexPath:indexPath];
			if(startCell==nil){
                startCell =[[MarkerViewTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:headerPlayerCell];
            }
            return startCell;
            break;
        case 1:
            startCell= [tableView dequeueReusableCellWithIdentifier:cell_NoOfHoles forIndexPath:indexPath];
            startCell.indexPathData=indexPath;
            if(startCell==nil){
                startCell =[[MarkerViewTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_NoOfHoles];
            }
//            [startCell updateCellData:noOfHoles andindexpath:indexPath];
			[cells_Array addObject:startCell];
            [startCell updateCellData:[_noOfHoles objectAtIndex:indexPath.row]];
            return startCell;
            break;
        default:
            startCell= [tableView dequeueReusableCellWithIdentifier:cell_IOTotal forIndexPath:indexPath];
			
            if(startCell==nil){
                startCell =[[MarkerViewTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_IOTotal];
            }
//            [startCell updateInOutTotalCell:inOutTotal andindexpath:indexPath];
            [startCell updateInOutTotalCell:[inOutTotal objectAtIndex:indexPath.row]];
            return startCell;
            break;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if(section==0)
        return 1;
    else if(section==1)
        return [_noOfHoles count];
    else
        return inOutTotal.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 3;
}

- (IBAction)tapGestureAction:(id)sender {
	[self.view endEditing:true];
}



@end
