//
//  MarkerViewTableCell.h
//  BirdieGolf
//
//  Created by Approutes on 18/04/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MarkerViewTableCell : UITableViewCell
{
	int count;
}
@property (strong, nonatomic) IBOutlet UITextField *txtFieldOneHole;
@property (strong, nonatomic) IBOutlet UILabel *noOfHolesLbl;
@property (strong, nonatomic) IBOutlet UILabel *iOTotalLbl;
@property (strong, nonatomic) IBOutlet UITextField *txtField2Hole;
@property (strong, nonatomic) IBOutlet UITextField *txtFieldZeroHole;
@property (strong, nonatomic) IBOutlet UITextField *txtFld2IOTotal;
@property (strong, nonatomic) IBOutlet UITextField *txtFldZeroIOTotal;


@property(strong,nonatomic) NSIndexPath* indexPathData;

//-(void)updateCellData:(id)inGolfHole andindexpath:(NSIndexPath *)inIndexPath;
- (void)updateCellData:(id)inGolfHole;

//-(void)updateInOutTotalCell:(id)inIOTotal andindexpath:(NSIndexPath *)inIndexPath;
- (void)updateInOutTotalCell:(id)inIOTotal;
@end
