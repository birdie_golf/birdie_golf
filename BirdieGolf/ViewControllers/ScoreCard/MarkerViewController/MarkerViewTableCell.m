//
//  MarkerViewTableCell.m
//  BirdieGolf
//
//  Created by Approutes on 18/04/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import "MarkerViewTableCell.h"
#import "CMMarkerVC.h"
#import "DataManager.h"


@interface MarkerViewTableCell()<UITextFieldDelegate>
{
	CMMarkerVC *aCMMarker;
	DataManager *aDataManager;
}
@end

@implementation MarkerViewTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	[[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(didChangedTextField:) name:UITextFieldTextDidEndEditingNotification object: nil];
	count=0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)updateCellData:(id)inGolfHole
{
	aCMMarker=inGolfHole;
	_noOfHolesLbl.text=aCMMarker.noOfHolesStr;
	_txtFieldOneHole.text=aCMMarker.txt_1_FieldStr;
	_txtFieldZeroHole.text=aCMMarker.txt_0_FieldStr;
	_txtField2Hole.text=aCMMarker.txt_2_FieldStr;
    _noOfHolesLbl.textColor=[UIColor colorWithRed:13.0/255.0 green:85.0/255.0 blue:136.0/255.0 alpha:1];

}

- (void)updateInOutTotalCell:(id)inIOTotal
{
    _iOTotalLbl.text=inIOTotal;
    _iOTotalLbl.textColor=[UIColor colorWithRed:13.0/255.0 green:85.0/255.0 blue:136.0/255.0 alpha:1];
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
	
	_txtFieldZeroHole.delegate = self;
	_txtFieldOneHole.delegate=self;
	_txtField2Hole.delegate = self;
	
	return self;
}

-(void)didChangedTextField:(NSNotification *)inNotification{
	if ([[inNotification name] isEqualToString: UITextFieldTextDidEndEditingNotification]) {
//		if (count%3==0) {
//			
//			aCMMarker=[[CMMarkerVC alloc]init];
//		}
		UITextField *aText = inNotification.object;
		if ([aText isEqual: _txtFieldZeroHole]) {
			aCMMarker.txt_0_FieldStr=aText.text;
		}
		if ([aText isEqual: _txtFieldOneHole]) {
			aCMMarker.txt_1_FieldStr=aText.text;
		}
		if ([aText isEqual: _txtField2Hole]) {
			aCMMarker.txt_2_FieldStr=aText.text;
		}
	}
	
//	++count;
//	
//	if (count%3==0) {
//		
//		[self fillSharedArray:aCMMarker];
//	}

}

- (void)fillSharedArray:(CMMarkerVC*)inCMMarker
{
	if ([inCMMarker.txt_0_FieldStr length]>0 && [inCMMarker.txt_1_FieldStr length]>0  && [inCMMarker.txt_2_FieldStr length]>0 )
	{
		[[DataManager dataManager].sharedArrayMarker addObject:aCMMarker];
	}
	else
	{
//		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!!!"
//														message:@"Please Enter Valid Data"
//													   delegate:self
//											  cancelButtonTitle:@"OK"
//											  otherButtonTitles:nil];
//		[alert show];
	}
	
}
@end
