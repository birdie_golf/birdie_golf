//
//  CMMemberVC.h
//  BirdieGolf
//
//  Created by Approutes on 10/05/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CMMemberVC : NSObject 
@property (strong, nonatomic) NSMutableArray *arrayCmMember;

@property (strong, nonatomic) NSString *strLblGolfHoles;
@property (strong, nonatomic) NSString *strTxtMeters;
@property (strong,   nonatomic) NSString *strTxtPar;
@property (strong,   nonatomic) NSString *strTxtIndex;
- (id)initWithResponse:(id)response;
@end
