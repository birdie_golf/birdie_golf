//
//  CMMemberVC.m
//  BirdieGolf
//
//  Created by Approutes on 10/05/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import "CMMemberVC.h"
#import "MembersGolfHoleTableCell.h"
@implementation CMMemberVC
- (id)initWithResponse:(id)response
{
	self = [super init];
	
	if (self) {
		
		if (response)// && [response count]>0)
		{
			[self textFieldData:response];
		}
	}
	return self;
}
-(void)textFieldData:(id)responseObject
{
	MembersGolfHoleTableCell *cell=responseObject;
	_strLblGolfHoles=cell.lblGolfHoles.text;
	_strTxtMeters=cell.txtMeters.text;
	_strTxtPar=cell.txtPar.text;
	_strTxtIndex=cell.txtIndex.text;
	NSLog(@"_strLblGolfHoles : %@ _strTxtMeters : %@ _strTxtPar : %@ _strTxtIndex : %@",_strLblGolfHoles,_strTxtMeters,_strTxtPar,_strTxtIndex);
	
}

@end
