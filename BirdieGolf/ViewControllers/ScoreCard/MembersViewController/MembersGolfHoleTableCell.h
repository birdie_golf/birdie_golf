//
//  TVCForGolfHole.h
//  BirdieGolf
//
//  Created by AppRoutes on 24/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MembersViewController.h"
#import "CMMemberVC.h"

@interface MembersGolfHoleTableCell : UITableViewCell
{
	int count;
}
@property (strong, nonatomic) IBOutlet UILabel     *lblGolfHoles;
@property (strong, nonatomic) IBOutlet UITextField *txtMeters;
@property (strong, nonatomic) IBOutlet UITextField *txtPar;
@property (strong, nonatomic) IBOutlet UITextField *txtIndex;
@property (strong, nonatomic)          NSIndexPath *indexPathData;
@property (strong, nonatomic) IBOutlet UILabel     *inOutTotalLbl;

@property (strong, nonatomic) CMMemberVC *aCMMemberVC;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier ;

- (void)updateCellData:(id)inGolfHole;

- (void)updateInOutTotalCell:(id)inIOTotal;



@end
