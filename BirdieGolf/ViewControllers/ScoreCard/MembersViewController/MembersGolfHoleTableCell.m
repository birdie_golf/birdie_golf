//
//  TVCForGolfHole.m
//  BirdieGolf
//
//  Created by AppRoutes on 24/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import "MembersGolfHoleTableCell.h"

#import "ScoreCardViewController.h"
#import "DataManager.h"

@interface MembersGolfHoleTableCell()<UITextFieldDelegate>
{
//	CMMemberVC *aCMMemberVC;
	
}

@end


@implementation MembersGolfHoleTableCell

@synthesize aCMMemberVC;

- (void)awakeFromNib {
	
	count=0;
    // Initialization code
		[[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(didChangedTextField:) name:UITextFieldTextDidChangeNotification object: nil];
	
	
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (void)updateCellData:(id)inGolfHole
{
	self.aCMMemberVC = inGolfHole;
	
	_lblGolfHoles.text= self.aCMMemberVC.strLblGolfHoles;
	_txtMeters.text=self.aCMMemberVC.strTxtMeters;
	_txtPar.text=self.aCMMemberVC.strTxtPar;
	_txtIndex.text=self.aCMMemberVC.strTxtIndex;
	_lblGolfHoles.textColor=[UIColor colorWithRed:13.0/255.0 green:85.0/255.0 blue:136.0/255.0 alpha:1];
}
- (void)updateInOutTotalCell:(id)inIOTotal
{
    _inOutTotalLbl.text=inIOTotal;
    _inOutTotalLbl.textColor=[UIColor colorWithRed:13.0/255.0 green:85.0/255.0 blue:136.0/255.0 alpha:1];

}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
 
	if (self) {
		
		CGSize size = self.contentView.frame.size;
		_lblGolfHoles=[[UILabel alloc]initWithFrame:CGRectMake(24.0, 4.0, size.width, size.height)];
		_txtMeters=[[UITextField alloc]initWithFrame:CGRectMake(95.0, 5.0, size.width, size.height)];
		_txtPar=[[UITextField alloc]initWithFrame:CGRectMake(172.0, 5.0, size.width, size.height)];
		_txtIndex=[[UITextField alloc]initWithFrame:CGRectMake(257.0, 5.0, size.width, size.height)];
		[self.contentView addSubview:_lblGolfHoles];
		[self.contentView addSubview:_txtMeters];
		[self.contentView addSubview:_txtPar];
		[self.contentView addSubview:_txtIndex];
		_txtMeters.delegate = self;
		_txtPar.delegate = self;
		_txtIndex.delegate = self;

	}
	return self;
}

-(void)didChangedTextField:(NSNotification *)inNotification{
	if ([[inNotification name] isEqualToString: UITextFieldTextDidChangeNotification]) {
		
//		if (count%3==0) {
//			
//			aCMMemberVC=[[CMMemberVC alloc]init];
//		}
//		self.aCMMemberVC = [[DataManager dataManager].sharedArrayMember objectAtIndex: self.indexPathData.row];
		UITextField *aText = inNotification.object;
		if ([aText isEqual: _txtMeters]) {
			self.aCMMemberVC.strTxtMeters=aText.text;
		}
		if ([aText isEqual: _txtPar]) {
			self.aCMMemberVC.strTxtPar=aText.text;
		}
		if ([aText isEqual: _txtIndex]) {
			self.aCMMemberVC.strTxtIndex=aText.text;
		}
	}
	++count;
	
	if (count%3==0) {
		
		//[self fillSharedArray:aCMMemberVC];
	}
	
}

- (void)fillSharedArray:(CMMemberVC*)inCMMember
{
	if ([inCMMember.strTxtMeters length]>0 && [inCMMember.strTxtMeters length]>0 && [inCMMember.strTxtMeters length]>0)
	{
		[[DataManager dataManager].sharedArrayMember addObject:aCMMemberVC];
	}
	else
	{
		
		if (count%3==0) {
			
//			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!!!"
//															message:@"Please Enter Valid Data"
//														   delegate:self
//												  cancelButtonTitle:@"OK"
//												  otherButtonTitles:nil];
//			[alert show];
//
			}

		}	
}

@end









