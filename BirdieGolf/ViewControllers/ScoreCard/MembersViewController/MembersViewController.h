//
//  membersViewController.h
//  BirdieGolf
//
//  Created by AppRoutes on 22/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MembersGolfHoleTableCell.h"
#import "CMGolfCourseNameModel.h"
#import "ScoreCardViewController.h"
#import "CMMemberVC.h"

@interface MembersViewController : UIViewController <UITextFieldDelegate>
{
    NSMutableArray*  list;
	

}
@property (strong, nonatomic) IBOutlet UITableView *memberTableView;
@property (strong, nonatomic) CMGolfCourseNameModel *aCMGolfCourseNameModel;
@property (strong, nonatomic) NSString *noOfHolesInGolfCourse;
@property (strong, nonatomic) NSMutableArray *noOfHoles;
//@property (nonatomic)		  NSInteger length;
@property (strong, nonatomic) CMMemberVC *aCmMember;
@property (assign, nonatomic) BOOL isAppear;
- (IBAction)tapGestureAction:(id)sender;


@end
