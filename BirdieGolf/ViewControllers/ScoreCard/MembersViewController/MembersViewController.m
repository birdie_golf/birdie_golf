//
//  membersViewController.m
//  BirdieGolf
//
//  Created by AppRoutes on 22/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import "MembersViewController.h"
#import "ScoreCardViewController.h"
#import "CMGolfCourseNameModel.h"
#import "CMMemberVC.h"
@interface MembersViewController () <ScoreCardDelegate>
{
    NSMutableArray *inOutTotal;
    NSMutableArray *arrayForSection;
	NSMutableArray *cellsArray;
	ScoreCardViewController *scoreCardVC;
	NSMutableArray *arrayCMMember;
}
@end

@implementation MembersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
	[self NoOfRowInThirdSectionOfTableView];
	arrayCMMember=[[NSMutableArray alloc]init];
	cellsArray=[[NSMutableArray alloc]init];
	scoreCardVC.delegate=self;
}

- (void)NoOfRowInThirdSectionOfTableView
{
	if ([_noOfHoles count]>9) {
		inOutTotal=[NSMutableArray arrayWithObjects:@"In",@"Out",@"Total", nil];
	}
	else
	{
		inOutTotal=[NSMutableArray arrayWithObjects:@"Total", nil];
	}
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

    // Dispose of any resources that can be recreated.
}
- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	if (!_isAppear) {
	[_memberTableView reloadData];
	}
}
- (void)didViewReAppear:(BOOL)isReappear
{
	if (isReappear) {
		[_memberTableView reloadData];
	}
}

#pragma mark-TableView Cell
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *headerCell=@"headerCell";
    static NSString *holeCell = @"holeCell";
    static NSString *additonCell=@"additonCell";
    MembersGolfHoleTableCell *startCell = nil;
//    startCell.indexPathData=indexPath;
    switch (indexPath.section) {
        case 0:
            startCell= [tableView dequeueReusableCellWithIdentifier:headerCell forIndexPath:indexPath];
            if(startCell==nil){
                startCell =[[MembersGolfHoleTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:headerCell];
            }
            break;
        case 1:
            startCell= [tableView dequeueReusableCellWithIdentifier:holeCell forIndexPath:indexPath];
            startCell.indexPathData=indexPath;
//            if(startCell==nil)
//			{
//				startCell =[[MembersGolfHoleTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:holeCell];
//			}
			
			[startCell updateCellData:[_noOfHoles objectAtIndex:indexPath.row]];
//			[cellsArray addObject:startCell];
//            return startCell;
            break;
        default:
            startCell= [tableView dequeueReusableCellWithIdentifier:additonCell forIndexPath:indexPath];
            if(startCell==nil){
                startCell =[[MembersGolfHoleTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:additonCell];
            }
            [startCell updateInOutTotalCell:[inOutTotal objectAtIndex:indexPath.row]];
		
//            return startCell;
            break;
        }
	return startCell;

}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if(section==0)
        return 1;
	else if(section==1)
			return [_noOfHoles count];
    else
    return inOutTotal.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (IBAction)tapGestureAction:(id)sender {
	[self.view endEditing:true];
}


@end
