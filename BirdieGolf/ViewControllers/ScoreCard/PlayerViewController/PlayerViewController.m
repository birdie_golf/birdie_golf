//
//  playerViewController.m
//  BirdieGolf
//
//  Created by AppRoutes on 22/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import "PlayerViewController.h"
#import "PlayerViewTableCell.h"
#import "CMPlayerVC.h"

@interface PlayerViewController ()
{
    NSMutableArray *inOutTotal;
	NSMutableArray *cells_Array;
	NSMutableArray *arrayCMPlayer;
//    NSMutableArray *noOfHoles;
//    NSInteger length;
}
@end

@implementation PlayerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    inOutTotal=[NSMutableArray arrayWithObjects:@"In",@"Out",@"Total", nil];
	[self NoOfRowInThirdSectionOfTableView];
	cells_Array=[[NSMutableArray alloc]init];
	arrayCMPlayer=[[NSMutableArray alloc]init];


//    noOfHoles=[NSMutableArray arrayWithObjects:@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9", nil];

}
- (void)NoOfRowInThirdSectionOfTableView
{
	if ([_noOfHoles count]>9) {
		inOutTotal=[NSMutableArray arrayWithObjects:@"In",@"Out",@"Total", nil];
	}
	else
	{
		inOutTotal=[NSMutableArray arrayWithObjects:@"Total", nil];
	}
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	if (!_isAppear) {
	[_playerTableView reloadData];
	}

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *headerPlayerCell=@"headerPlayerCell";
    static NSString *cell_NoOfHoles = @"Cell_NoOfHoles";
    static NSString *cell_InOutTotal=@"Cell_InOutTotal";
    PlayerViewTableCell *startCell;
    startCell.indexPathData=indexPath;
    switch (indexPath.section) {
        case 0:
            startCell= [tableView dequeueReusableCellWithIdentifier:headerPlayerCell forIndexPath:indexPath];			
            if(startCell==nil){
                startCell =[[PlayerViewTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:headerPlayerCell];
            }
            return startCell;
            break;
        case 1:
            startCell= [tableView dequeueReusableCellWithIdentifier:cell_NoOfHoles forIndexPath:indexPath];
            startCell.indexPathData=indexPath;
            if(startCell==nil){
                startCell =[[PlayerViewTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_NoOfHoles];
            }
			[cells_Array addObject:startCell];
            [startCell updateCellData:[_noOfHoles objectAtIndex:indexPath.row]];
            
            return startCell;
            break;
        default:
            startCell= [tableView dequeueReusableCellWithIdentifier:cell_InOutTotal forIndexPath:indexPath];
            if(startCell==nil){
                startCell =[[PlayerViewTableCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cell_InOutTotal];
            }
            [startCell updateInOutTotalCell:[inOutTotal objectAtIndex:indexPath.row]];
            return startCell;
            break;
    }
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    if(section==0)
        return 1;
    else if(section==1)
		return [_noOfHoles count];
    else
        return inOutTotal.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 3;
}


- (IBAction)tapGestureAction:(id)sender {
	[self.view endEditing:true];
}

@end
