//
//  PlayerViewTableCell.h
//  BirdieGolf
//
//  Created by Approutes on 18/04/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayerViewTableCell : UITableViewCell
{
	int count;
}
@property (strong, nonatomic) IBOutlet UILabel *noOfHoles;
@property (strong, nonatomic) IBOutlet UITextField *txt_1_Field;
@property (strong, nonatomic) IBOutlet UITextField *txt_2_Field;
@property (strong, nonatomic) IBOutlet UITextField *txt_0_Field;
@property (strong, nonatomic) IBOutlet UILabel *inOutTotalLbl;
@property (strong, nonatomic) IBOutlet UITextField *txt_IOTotal_2_Field;
@property (strong, nonatomic) IBOutlet UITextField *txt_IOTotal_0_Field;
@property(strong,nonatomic) NSIndexPath* indexPathData;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier ;
- (void)updateCellData:(id)inGolfHole;
- (void)updateInOutTotalCell:(id)inIOTotal;
@end
