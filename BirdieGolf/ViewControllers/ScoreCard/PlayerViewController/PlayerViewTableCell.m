//
//  PlayerViewTableCell.m
//  BirdieGolf
//
//  Created by Approutes on 18/04/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import "PlayerViewTableCell.h"
#import "CMPlayerVC.h"
#import "DataManager.h"


@interface PlayerViewTableCell()<UITextFieldDelegate>
{
	CMPlayerVC *aCMPlayerVC;
	DataManager *aDataManager;
}
@end
@implementation PlayerViewTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
	[[NSNotificationCenter defaultCenter] addObserver: self selector:@selector(didChangedTextField:) name:UITextFieldTextDidEndEditingNotification object: nil];
	count=0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void)updateCellData:(id)inGolfHole
{
	aCMPlayerVC=inGolfHole;
	_noOfHoles.text=aCMPlayerVC.noOfHolesStr;
	_txt_1_Field.text=aCMPlayerVC.txt_1_FieldStr;
	_txt_0_Field.text=aCMPlayerVC.txt_0_FieldStr;
	_txt_2_Field.text=aCMPlayerVC.txt_2_FieldStr;
    _noOfHoles.textColor=[UIColor colorWithRed:13.0/255.0 green:85.0/255.0 blue:136.0/255.0 alpha:1];

}
- (void)updateInOutTotalCell:(id)inIOTotal
{
    _inOutTotalLbl.text=inIOTotal;
    _inOutTotalLbl.textColor=[UIColor colorWithRed:13.0/255.0 green:85.0/255.0 blue:136.0/255.0 alpha:1];
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];

	_txt_0_Field.delegate = self;
	_txt_1_Field.delegate=self;
	_txt_2_Field.delegate = self;
	
	return self;
}
-(void)didChangedTextField:(NSNotification *)inNotification{
	if ([[inNotification name] isEqualToString: UITextFieldTextDidEndEditingNotification]) {
//		if (count%3==0) {
//			
//			aCMPlayerVC=[[CMPlayerVC alloc]init];
//		}
		UITextField *aText = inNotification.object;
		if ([aText isEqual: _txt_0_Field]) {
			aCMPlayerVC.txt_0_FieldStr=aText.text;
		}
		if ([aText isEqual: _txt_2_Field]) {
			aCMPlayerVC.txt_2_FieldStr=aText.text;
		}
		if ([aText isEqual: _txt_1_Field]) {
			aCMPlayerVC.txt_1_FieldStr=aText.text;
		}
	}
	
//	++count;
//	
//	if (count%3==0) {
//		
//		[self fillSharedArray:aCMPlayerVC];
//	}
}

- (void)fillSharedArray:(CMPlayerVC*)inaCMPlayer
{
	if ([inaCMPlayer.txt_0_FieldStr length]>0 && [inaCMPlayer.txt_1_FieldStr length]>0 && [inaCMPlayer.txt_2_FieldStr length]>0 )
	{
		[[DataManager dataManager].sharedArrayPlayer addObject:aCMPlayerVC];
	}
	else
	{
//		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!!!"
//														message:@"Please Enter Valid Data"
//													   delegate:self
//											  cancelButtonTitle:@"OK"
//											  otherButtonTitles:nil];
//		[alert show];
	}
	
}
@end
