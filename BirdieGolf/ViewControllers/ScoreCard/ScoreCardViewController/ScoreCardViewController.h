//
//  scoreCardViewController.h
//  Birdie Golf
//
//  Created by AppRoutes on 20/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//


#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "MembersViewController.h"
#import "PlayerViewController.h"
#import "LadiesViewController.h"
#import "CMGolfCourseNameModel.h"
#import "CMMemberVC.h"


@import MessageUI;

@protocol ScoreCardDelegate <NSObject>
@optional
- (void)didSelectedSegment:(UISegmentedControl *)indexPath;
- (void)didSendNumberOfHole:(NSString *)inNoOfHoles;
- (void)didViewReAppear:(BOOL)isReappear;
@end


@interface ScoreCardViewController : UIViewController <MFMailComposeViewControllerDelegate>
@property (strong, nonatomic) IBOutlet UIView *scoreCardVC;
@property (strong, nonatomic) IBOutlet UIView *memberContainer;
@property (strong, nonatomic) IBOutlet UIView *playerContainer;
@property (strong, nonatomic) IBOutlet UIView *markerContainer;
@property (strong, nonatomic) IBOutlet UIView *ladiesContainer;
@property (strong, nonatomic) IBOutlet UIButton *sendAsMailButton;


@property(nonatomic,weak) id <ScoreCardDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIView *otherContentView;

@property (strong, nonatomic) IBOutlet UISegmentedControl *typeSegmentControl;
@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) NSString* noOfHoleStr;
@property (strong, nonatomic) NSMutableArray* noOfHolesArray;
@property (strong, nonatomic) CMGolfCourseNameModel *aCMGolfCourseNameModel;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *segmentTopCons;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *otherContainerTopCons;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *otherContainerBottomCons;
@property (strong, nonatomic) IBOutlet NSLayoutConstraint *sendAsMailTopOtherContainerCons;


@property (strong, nonatomic) NSMutableArray *htmlDataArray;
@property (strong, nonatomic) CMMemberVC *aCMMemberVC;
- (IBAction)segmentValueChange:(id)sender;
//@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) IBOutlet UIWebView *webView;
//@property (nonatomic, strong) NDHTMLtoPDF *PDFCreator;
- (IBAction)sendAsMailBtn:(id)sender;



@end
