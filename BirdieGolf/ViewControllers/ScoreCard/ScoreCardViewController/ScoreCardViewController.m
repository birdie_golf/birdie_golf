//
//  scoreCardViewController.m
//  Birdie Golf
//
//  Created by AppRoutes on 20/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import "ScoreCardViewController.h"
#import "MembersViewController.h"
#import "PlayerViewController.h"
#import "MarkerViewController.h"
#import "LadiesViewController.h"
#import "MainCollectionViewController.h"
#import "MembersGolfHoleTableCell.h"
#import "CMPlayerVC.h"
#import "CMMarkerVC.h"
#import "CMLadiesVC.h"
#import "DataManager.h"
#import "UIPrintPageRenderer+PDF.h"
#define kPaperSizeA4 CGSizeMake(595.2,841.8)

@interface ScoreCardViewController () <UIGestureRecognizerDelegate,UIWebViewDelegate,UIAlertViewDelegate>
{
//    ScoreCardViewController *scoreCardVC;
    MembersViewController* memberVC;
    PlayerViewController *playerVC;
    MarkerViewController *markerVC;
    LadiesViewController *ladiesVC;
	NSString *flagRightSwipe;
	NSString *flagLeftSwipe;
	NSMutableArray* arrayNoOfHolesPlayerVC;
	NSMutableArray* arrayNoOfHolesMarkerVC;
	NSMutableArray* arrayNoOfHolesLadiesVC;
	MembersGolfHoleTableCell *aMemberGolf;
	DataManager *aDataManager;
	
}
@end

@implementation ScoreCardViewController
{
//    MainCollectionViewController *mainCollectionVC;
}
- (void)viewDidLoad {
    [super viewDidLoad];
	
//	NSMutableArray* temp=[DataManager dataManager].sharedArray;
    self.navigationItem.title=@"Score Card";
    self.navigationItem.hidesBackButton=NO;
	flagRightSwipe=@"1";
	flagLeftSwipe=@"1";
//    [self customBtn];
    memberVC=[[MembersViewController alloc]init];
    playerVC=[[PlayerViewController alloc]init];
    markerVC=[[MarkerViewController alloc]init];
    ladiesVC=[[LadiesViewController alloc]init];
		[self showMemberContainer];

	[self customLeftBarBtnImage];
	_htmlDataArray =[[NSMutableArray alloc]init];
	
//	NSLog(@"Number of holes %@",_noOfHole);

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
	[self.view endEditing:YES];
	[self showMemberContainer];
	_typeSegmentControl.selectedSegmentIndex=0;
	flagRightSwipe=@"1";
	_segmentTopCons.constant=0;
	[_typeSegmentControl layoutIfNeeded];
	_otherContainerTopCons.constant=29;
	_otherContainerBottomCons.constant=62;
	_sendAsMailTopOtherContainerCons.constant=8;
	[_otherContentView layoutIfNeeded];
	[_sendAsMailButton layoutIfNeeded];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewDidDisappear:(BOOL)animated
{
	[[DataManager dataManager].sharedArrayMember removeAllObjects];
	[[DataManager dataManager].sharedArrayPlayer removeAllObjects];
	[[DataManager dataManager].sharedArrayMarker removeAllObjects];
}
- (void)customLeftBarBtnImage
{
    UIButton *backBtn =[UIButton buttonWithType:UIButtonTypeCustom];
	UIImage *imageBackBtn=[UIImage imageNamed:@"backIconWhite"];
	[backBtn setFrame:CGRectMake(0.0f,0.0f,imageBackBtn.size.width, imageBackBtn.size.height)];
	[backBtn setBackgroundImage:imageBackBtn forState:UIControlStateNormal];
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:backBtn]];
    [backBtn addTarget:self action:@selector(popToViewController) forControlEvents:UIControlEventTouchUpInside];
}
- (void)popToViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)segmentValueChange:(id)sender
{
	[self.view endEditing:YES];
    switch (_typeSegmentControl.selectedSegmentIndex)
    {
        case 0:
			[self showMemberContainer];
            break;
        case 1:
			[self showPlayerContainer];
            break;
        case 2:
			[self showMarkerContainer];
            break;
        case 3:
			[self showLadiesContainer];
            break;
        default:
            break;
    }
}
- (IBAction)leftSwipeGesture:(id)sender {
	[self.view endEditing:YES];
	if (UISwipeGestureRecognizerDirectionRight && [flagLeftSwipe isEqual:@"1"]) {

		[self showMemberContainer];
		_typeSegmentControl.selectedSegmentIndex=0;
		flagRightSwipe=@"1";
	}
	else if (UISwipeGestureRecognizerDirectionRight && [flagLeftSwipe isEqual:@"2"]) {

		[self showPlayerContainer];
		_typeSegmentControl.selectedSegmentIndex=1;
		flagLeftSwipe=@"1";
		flagRightSwipe=@"2";
	}
	else if (UISwipeGestureRecognizerDirectionRight && [flagLeftSwipe isEqual:@"3"]) {

		[self showMarkerContainer];
		_typeSegmentControl.selectedSegmentIndex=2;
		flagLeftSwipe=@"2";
		flagRightSwipe=@"3";
	}
}
- (IBAction)rightSwipeGesture:(id)sender {
	[self.view endEditing:YES];
	if (UISwipeGestureRecognizerDirectionLeft && [flagRightSwipe isEqual:@"1"]) {
		[self showPlayerContainer];
		_typeSegmentControl.selectedSegmentIndex=1;
		flagRightSwipe=@"2";
		flagLeftSwipe=@"1";
	}
	else if (UISwipeGestureRecognizerDirectionLeft && [flagRightSwipe isEqual:@"2"]) {
		[self showMarkerContainer];
		_typeSegmentControl.selectedSegmentIndex=2;
		flagRightSwipe=@"3";
		flagLeftSwipe=@"2";
	}
	else if (UISwipeGestureRecognizerDirectionLeft && [flagRightSwipe isEqual:@"3"]) {
		[self showLadiesContainer];
		_typeSegmentControl.selectedSegmentIndex=3;
			flagLeftSwipe=@"3";
	}
}
-(void)showMemberContainer
{
	self.memberContainer.hidden=NO;
	self.playerContainer.hidden=YES;
	self.markerContainer.hidden=YES;
	self.ladiesContainer.hidden=YES;
	self.webView.hidden=YES;
}
-(void)showPlayerContainer
{
	self.memberContainer.hidden=YES;
	self.playerContainer.hidden=NO;
	self.markerContainer.hidden=YES;
	self.ladiesContainer.hidden=YES;
	self.webView.hidden=YES;
}
-(void)showMarkerContainer
{
	self.memberContainer.hidden=YES;
	self.playerContainer.hidden=YES;
	self.markerContainer.hidden=NO;
	self.ladiesContainer.hidden=YES;
	self.webView.hidden=YES;
}
-(void)showLadiesContainer
{
	self.memberContainer.hidden=YES;
	self.playerContainer.hidden=YES;
	self.markerContainer.hidden=YES;
	self.ladiesContainer.hidden=NO;
	self.webView.hidden=YES;
}




- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
	if (_noOfHolesArray == nil) {
		[self fillDataInArray];
	}
	
	if ([[segue identifier] isEqualToString:@"MembersViewControllerSegue"]) {
		MembersViewController *aMembersVC = segue.destinationViewController;
		aMembersVC.noOfHoles=_noOfHolesArray;
		[[DataManager dataManager].sharedArrayMember addObjectsFromArray : _noOfHolesArray];
	}
	 if ([[segue identifier] isEqualToString:@"PlayerViewControllerSegue"]) {
		PlayerViewController *aPlayerVC = segue.destinationViewController;
		 aPlayerVC.noOfHoles=arrayNoOfHolesPlayerVC;
		 [[DataManager dataManager].sharedArrayPlayer addObjectsFromArray : arrayNoOfHolesPlayerVC];
	}
	 if ([[segue identifier] isEqualToString:@"MarkerViewControllerSegue"]) {
		MarkerViewController *aMarkerVC = segue.destinationViewController;
		 aMarkerVC.noOfHoles=arrayNoOfHolesMarkerVC;
		 [[DataManager dataManager].sharedArrayMarker addObjectsFromArray : arrayNoOfHolesMarkerVC];
		 
	}
	 if ([[segue identifier] isEqualToString:@"LadiesViewControllerSegue"]) {
		LadiesViewController *aLadiesVC = segue.destinationViewController;
		aLadiesVC.noOfHoles=arrayNoOfHolesLadiesVC;
		 [[DataManager dataManager].sharedArrayLadies addObjectsFromArray : arrayNoOfHolesLadiesVC];
	}

}
-(int)fillDataInArray
{
	int i;
	_noOfHolesArray=[[NSMutableArray alloc]init];
	arrayNoOfHolesPlayerVC=[[NSMutableArray alloc]init];
	arrayNoOfHolesMarkerVC=[[NSMutableArray alloc]init];
	arrayNoOfHolesLadiesVC=[[NSMutableArray alloc]init];
	for (i=0; i<[_noOfHoleStr intValue]; i++) {
		CMMemberVC *aCMMember=[[CMMemberVC alloc]init];
		CMPlayerVC *aCMPlayer =[[CMPlayerVC alloc]init];
		CMMarkerVC *aCMMarkerVC=[[CMMarkerVC alloc]init];
		CMLadiesVC *aCMLadiesVC=[[CMLadiesVC alloc]init];
		
		aCMMember.strLblGolfHoles=[NSString stringWithFormat:@"%d",i+1];
		
		[_noOfHolesArray addObject:aCMMember];
		[arrayNoOfHolesPlayerVC addObject:aCMPlayer];
		[arrayNoOfHolesMarkerVC addObject:aCMMarkerVC];
		[arrayNoOfHolesLadiesVC addObject:aCMLadiesVC];
	}
	
	return i;
}


- (IBAction)sendAsMailBtn:(id)sender
{
//	UIAlertView *alert = [[UIAlertView alloc]
//						  initWithTitle:@"Congratulations, You Got A New High Score!!"
//						  message:@"Want to mail Score card details"
//						  delegate:self // <== changed from nil to self
//						  cancelButtonTitle:@"Cancel"
//						  otherButtonTitles:@"Ok", nil];
	
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Congratulations, You Got A New High Score!!"
										  message:@"Want to mail Score card details"
										  delegate:self
										  cancelButtonTitle:@"Cancel"
										  otherButtonTitles:@"Ok",nil];
	[alert show];
//	NSMutableString *stringToHtml=[self updateHtml];
//
////	[_webView setFrame:CGRectMake(0, 100, 320, 414)];
//	self.webView=[[UIWebView alloc]init];
//	self.webView.delegate=self;
//	self.webView.hidden=NO;	
//	NSString *path = [[NSBundle mainBundle] bundlePath];
//	NSURL *baseURL = [NSURL fileURLWithPath:path];
//	[_webView loadHTMLString:stringToHtml baseURL:baseURL];
//	memberVC.isAppear=NO;
//	playerVC.isAppear=NO;
//	markerVC.isAppear=NO;
//	ladiesVC.isAppear=NO;
//	
//	if (self.delegate && [self.delegate conformsToProtocol:@protocol(ScoreCardDelegate)] && [self.delegate respondsToSelector:@selector(didViewReAppear:)]) {
//		[self.delegate didViewReAppear:YES];
//	}
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	if (buttonIndex == [alertView firstOtherButtonIndex]) {
		NSMutableString *stringToHtml=[self updateHtml];
		
		//	[_webView setFrame:CGRectMake(0, 100, 320, 414)];
		self.webView=[[UIWebView alloc]init];
		self.webView.delegate=self;
		self.webView.hidden=NO;
		NSString *path = [[NSBundle mainBundle] bundlePath];
		NSURL *baseURL = [NSURL fileURLWithPath:path];
		[_webView loadHTMLString:stringToHtml baseURL:baseURL];
		memberVC.isAppear=NO;
		playerVC.isAppear=NO;
		markerVC.isAppear=NO;
		ladiesVC.isAppear=NO;
		
		if (self.delegate && [self.delegate conformsToProtocol:@protocol(ScoreCardDelegate)] && [self.delegate respondsToSelector:@selector(didViewReAppear:)]) {
			[self.delegate didViewReAppear:YES];
		}

	}
}
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
	NSData *pdfData ;
	if (_webView.isLoading)
		return;
//	[self performSelector:@selector(createPdf) withObject: nil afterDelay: 1.0];
	UIPrintPageRenderer *render = [[UIPrintPageRenderer alloc] init];
	[render addPrintFormatter:_webView.viewPrintFormatter startingAtPageAtIndex:0];
	//increase these values according to your requirement
	float topPadding = 10.0f;
	float bottomPadding = 10.0f;
	float leftPadding = 10.0f;
	float rightPadding = 10.0f;
	CGRect printableRect = CGRectMake(leftPadding,
									  topPadding,
									  kPaperSizeA4.width-leftPadding-rightPadding,
									  kPaperSizeA4.height-topPadding-bottomPadding);
	CGRect paperRect = CGRectMake(0, 0, kPaperSizeA4.width, kPaperSizeA4.height);
	[render setValue:[NSValue valueWithCGRect:paperRect] forKey:@"paperRect"];
	[render setValue:[NSValue valueWithCGRect:printableRect] forKey:@"printableRect"];
	pdfData = [render printToPDF];
	NSString *filePath=[NSString stringWithFormat:@"%@/ScoreCard.pdf",NSTemporaryDirectory()];
	NSFileManager *fileManager = [NSFileManager defaultManager];
	NSError *error;

	if (pdfData) {
		[pdfData writeToFile:filePath atomically: YES];
		
		NSLog(@"%@",[NSString stringWithFormat:@"%@/ScoreCard.pdf",NSTemporaryDirectory()]);
	}
	else
	{
		NSLog(@"PDF couldnot be created");
	}

	if ([MFMailComposeViewController canSendMail]) {
		
		// Email Subject
		NSString *emailTitle = @"Birdie Golf Mail";
		// Email Content
		NSString *messageBody = @"Your scorecard is right here!!!";
		// To address
		//		NSArray *toRecipents = [NSArray arrayWithObject:@"support@appcoda.com"];
		//		id sender;
		//		[self webViewAction:sender];
		MFMailComposeViewController *aMFMailComposeViewController = [[MFMailComposeViewController alloc] init];
		aMFMailComposeViewController.mailComposeDelegate = self;
		[aMFMailComposeViewController setSubject:emailTitle];
		[aMFMailComposeViewController setMessageBody:messageBody isHTML:NO];
		//		[aMFMailComposeViewController setToRecipients:toRecipents];
		
		//		UIImage *myImage = [UIImage imageNamed:@"homeImageThree.png"];
		//		NSData *imageData = UIImagePNGRepresentation(myImage);
		
		[aMFMailComposeViewController addAttachmentData:pdfData mimeType:@"application/pdf" fileName:@"ScoreCard.pdf"];
		
		[self presentViewController:aMFMailComposeViewController animated:YES completion:NULL];
		
	}
	else
	{
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failure"
														message:@"Your device doesn't support the composer sheet"
													   delegate:nil
											  cancelButtonTitle:@"OK"
											  otherButtonTitles:nil];
		[alert show];
	}
	[fileManager removeItemAtPath:filePath error:&error];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(nullable NSError *)error
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!!!"
												message:@"Please Enter Valid Data"
						  						delegate:self
						  						cancelButtonTitle:@"OK"
						  						otherButtonTitles:nil];
	[alert show];
	
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
	switch (result) {
  case MFMailComposeResultCancelled:
			NSLog(@"Mail cancelled");
			memberVC.isAppear=NO;
			playerVC.isAppear=NO;
			markerVC.isAppear=NO;
			ladiesVC.isAppear=NO;

			break;
		case MFMailComposeResultSaved:
			NSLog(@"Mail saved");
			break;
		case MFMailComposeResultSent:
			NSLog(@"Mail sent");
			memberVC.isAppear=NO;
			playerVC.isAppear=NO;
			markerVC.isAppear=NO;
			ladiesVC.isAppear=NO;

			break;
		case MFMailComposeResultFailed:
			NSLog(@"Mail sent failure: %@", [error localizedDescription]);
			break;
  default:
			break;
	}
	[self dismissViewControllerAnimated:YES completion:NULL];
}

-(NSMutableString*)updateHtml
{
	NSError* error = nil;
	NSString *pathHtml = [[NSBundle mainBundle] pathForResource: @"index" ofType: @"html"];
	NSString *finalHtml = [NSString stringWithContentsOfFile: pathHtml encoding:NSUTF8StringEncoding error: &error];
	NSMutableString *editStr=[[NSMutableString alloc]initWithString:@"<tr><td>noOfHole</td><td>memberMeters</td><td>memberPar</td><td>memberIndex</td><td>playerOne</td><td>playerTwo</td><td>playerZero</td><td>markerOne</td><td>markerTwo</td><td>markerZero</td><td>ladiesMeters</td><td>ladiesPar</td><td>ladiesIndex</td></tr>"];
	NSString *editedStr=[[NSString alloc]init];
	NSMutableString *htmlAppendedStr=[[NSMutableString alloc]init];

		NSMutableArray* tempArrayMember=[DataManager dataManager].sharedArrayMember;
		NSMutableArray* tempArrayPlayer=[DataManager dataManager].sharedArrayPlayer;
		NSMutableArray* tempArrayMarker=[DataManager dataManager].sharedArrayMarker;
		NSMutableArray* tempArrayLadies=[DataManager dataManager].sharedArrayLadies;
	

	for (int i=0; i<[tempArrayMember count]; i++) { //[_noOfHoleStr intValue]
		
		NSLog(@"%@",[tempArrayMember objectAtIndex:i]);

//=========================== CMMember =============================================================
			CMMemberVC *aCMMember=[tempArrayMember objectAtIndex:i];
			if ([editStr containsString:@"noOfHole"]) {
//				htmlStr=(NSMutableString*)[htmlStr stringByReplacingOccurrencesOfString:@"noOfHole" withString:[NSString stringWithFormat:@"%d",i+1]];
				editedStr=(NSMutableString*)[editStr stringByReplacingOccurrencesOfString:@"noOfHole" withString:aCMMember.strLblGolfHoles];
			}
		
			if ([aCMMember.strTxtMeters length]>0) {
					if ([editedStr containsString:@"memberMeters"]) {
						editedStr=(NSMutableString*)[editedStr stringByReplacingOccurrencesOfString:@"memberMeters" withString:aCMMember.strTxtMeters];
					}
				}
				else
					editedStr=(NSMutableString*)[editedStr stringByReplacingOccurrencesOfString:@"memberMeters" withString:@""];

		
			if ([aCMMember.strTxtPar length]>0) {
				if ([editedStr containsString:@"memberPar"]) {
					editedStr=(NSMutableString*)[editedStr stringByReplacingOccurrencesOfString:@"memberPar" withString:aCMMember.strTxtPar];
				}
			}
			else
				editedStr=(NSMutableString*)[editedStr stringByReplacingOccurrencesOfString:@"memberPar" withString:@""];

			if ([aCMMember.strTxtIndex length]>0) {
				if ([editedStr containsString:@"memberIndex"]) {
					editedStr=(NSMutableString*)[editedStr stringByReplacingOccurrencesOfString:@"memberIndex" withString:aCMMember.strTxtIndex];
				}
			}
			else
				editedStr=(NSMutableString*)[editedStr stringByReplacingOccurrencesOfString:@"memberIndex" withString:@""];

		
//==================================================================================================
		
		
//=========================== CMPlayer =============================================================
			CMPlayerVC *aCMPlayer =[tempArrayPlayer objectAtIndex:i];
		
		if ([aCMPlayer.txt_1_FieldStr length]>0) {
			if ([editedStr containsString:@"playerOne"]) {
				editedStr=(NSMutableString*)[editedStr stringByReplacingOccurrencesOfString:@"playerOne" withString:aCMPlayer.txt_1_FieldStr];
			}
		}
		else
			editedStr=(NSMutableString*)[editedStr stringByReplacingOccurrencesOfString:@"playerOne" withString:@""];
		
		if ([aCMPlayer.txt_2_FieldStr length]>0) {
			if ([editedStr containsString:@"playerTwo"]) {
				editedStr=(NSMutableString*)[editedStr stringByReplacingOccurrencesOfString:@"playerTwo" withString:aCMPlayer.txt_2_FieldStr];
			}
		}
		else
			editedStr=(NSMutableString*)[editedStr stringByReplacingOccurrencesOfString:@"playerTwo" withString:@""];
		
		if ([aCMPlayer.txt_0_FieldStr length]>0) {
			if ([editedStr containsString:@"playerZero"]) {
				editedStr=(NSMutableString*)[editedStr stringByReplacingOccurrencesOfString:@"playerZero" withString:aCMPlayer.txt_0_FieldStr];
			}
		}
		
		else
			editedStr=(NSMutableString*)[editedStr stringByReplacingOccurrencesOfString:@"playerZero" withString:@""];
		
		
//==================================================================================================
		
//=========================== CMMarker =============================================================
			CMMarkerVC *aCMMarkerVC=[tempArrayMarker objectAtIndex:i];
		
		
		if ([aCMMarkerVC.txt_1_FieldStr length]>0) {
			if ([editedStr containsString:@"markerOne"]) {
				editedStr=(NSMutableString*)[editedStr stringByReplacingOccurrencesOfString:@"markerOne" withString:aCMMarkerVC.txt_1_FieldStr];
			}
		}
		else
			editedStr=(NSMutableString*)[editedStr stringByReplacingOccurrencesOfString:@"markerOne" withString:@""];
		
		if ([aCMMarkerVC.txt_2_FieldStr length]>0) {
			if ([editedStr containsString:@"markerTwo"]) {
				editedStr=(NSMutableString*)[editedStr stringByReplacingOccurrencesOfString:@"markerTwo" withString:aCMMarkerVC.txt_2_FieldStr];
			}
		}
		else
			editedStr=(NSMutableString*)[editedStr stringByReplacingOccurrencesOfString:@"markerTwo" withString:@""];
		
		if ([aCMMarkerVC.txt_0_FieldStr length]>0) {
			if ([editedStr containsString:@"markerZero"]) {
				editedStr=(NSMutableString*)[editedStr stringByReplacingOccurrencesOfString:@"markerZero" withString:aCMMarkerVC.txt_0_FieldStr];
			}
		}
		else
			editedStr=(NSMutableString*)[editedStr stringByReplacingOccurrencesOfString:@"markerZero" withString:@""];
		
		
//==================================================================================================
		
		
		
//=========================== CMLadies =============================================================
			CMLadiesVC *aCMLadiesVC=[tempArrayLadies objectAtIndex:i];
		
		if ([aCMLadiesVC.strTxtMeters length]>0) {
			if ([editedStr containsString:@"ladiesMeters"]) {
				editedStr=(NSMutableString*)[editedStr stringByReplacingOccurrencesOfString:@"ladiesMeters" withString:aCMLadiesVC.strTxtMeters];
			}
		}
		else
			editedStr=(NSMutableString*)[editedStr stringByReplacingOccurrencesOfString:@"ladiesMeters" withString:@""];
		
		if ([aCMLadiesVC.strTxtPar length]>0) {
			if ([editedStr containsString:@"ladiesPar"]) {
				editedStr=(NSMutableString*)[editedStr stringByReplacingOccurrencesOfString:@"ladiesPar" withString:aCMLadiesVC.strTxtPar];
			}
		}
		else
			editedStr=(NSMutableString*)[editedStr stringByReplacingOccurrencesOfString:@"ladiesPar" withString:@""];
		
		
		if ([aCMLadiesVC.strTxtIndex length]>0) {
			if ([editedStr containsString:@"ladiesIndex"]) {
				editedStr=(NSMutableString*)[editedStr stringByReplacingOccurrencesOfString:@"ladiesIndex" withString:aCMLadiesVC.strTxtIndex];
			}
		}
		else
			editedStr=(NSMutableString*)[editedStr stringByReplacingOccurrencesOfString:@"ladiesIndex" withString:@""];
		
		[htmlAppendedStr appendString:editedStr];
//==================================================================================================
		
	}
	if ([finalHtml containsString:@"<td>noOfHole</td><td>memberMeters</td><td>memberPar</td><td>memberIndex</td><td>playerOne</td><td>playerTwo</td><td>playerZero</td><td>markerOne</td><td>markerTwo</td><td>markerZero</td><td>ladiesMeters</td><td>ladiesPar</td><td>ladiesIndex</td>"]) {
		finalHtml=(NSMutableString*)[finalHtml stringByReplacingOccurrencesOfString:@"<td>noOfHole</td><td>memberMeters</td><td>memberPar</td><td>memberIndex</td><td>playerOne</td><td>playerTwo</td><td>playerZero</td><td>markerOne</td><td>markerTwo</td><td>markerZero</td><td>ladiesMeters</td><td>ladiesPar</td><td>ladiesIndex</td>" withString:htmlAppendedStr];
		
		NSLog(@"%@",finalHtml);
		
	}
	return (NSMutableString*)finalHtml;
}
@end


