//
//  UIPrintPageRenderer+PDF.h
//  BirdieGolf
//
//  Created by Approutes on 31/05/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIPrintPageRenderer (PDF)
- (NSData*) printToPDF;
@end
