//
//  StaticMapViewController.m
//  BirdieGolf
//
//  Created by Arpit Shukla on 29/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import "StaticMapViewController.h"

@interface StaticMapViewController ()

@end

@implementation StaticMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.hidesBackButton=NO;
    self.navigationItem.title=@"Static Map";
	[self customLeftBarBtnImage];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)customLeftBarBtnImage
{
	UIButton *backBtn =[UIButton buttonWithType:UIButtonTypeCustom];
	UIImage *imageBackBtn=[UIImage imageNamed:@"backIconWhite"];
	[backBtn setFrame:CGRectMake(0.0f,0.0f,imageBackBtn.size.width, imageBackBtn.size.height)];
	[backBtn setBackgroundImage:imageBackBtn forState:UIControlStateNormal];
	[self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:backBtn]];
	[backBtn addTarget:self action:@selector(popToViewController) forControlEvents:UIControlEventTouchUpInside];
}
- (void)popToViewController
{
	[self.navigationController popViewControllerAnimated:YES];
}


@end
