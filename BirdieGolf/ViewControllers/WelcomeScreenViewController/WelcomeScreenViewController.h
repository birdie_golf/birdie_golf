//
//  welcomeScreenViewController.h
//  Birdie Golf
//
//  Created by AppRoutes on 20/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface WelcomeScreenViewController : UIViewController
- (IBAction)nearYouBtn:(id)sender;
- (IBAction)australiaWideBtn:(id)sender;
@property(assign, nonatomic)BOOL isSelectedTableView;

@end
