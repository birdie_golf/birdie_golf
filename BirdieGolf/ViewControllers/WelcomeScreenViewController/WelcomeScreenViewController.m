//
//  welcomeScreenViewController.m
//  Birdie Golf
//
//  Created by AppRoutes on 20/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import "WelcomeScreenViewController.h"

@interface WelcomeScreenViewController ()

@end

@implementation WelcomeScreenViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title=@"Welcome";
    self.navigationItem.hidesBackButton=YES;
	
   }

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)nearYouBtn:(id)sender {
	
	HomeScreenViewController *homeScreenVC=[self.storyboard instantiateViewControllerWithIdentifier:@"HomeScreenViewController"];
    homeScreenVC.isEnable=YES;
    homeScreenVC.isDisable=NO;
	homeScreenVC.showHideSelection=@"1";
	[self.navigationController pushViewController:homeScreenVC animated:YES];
}

- (IBAction)australiaWideBtn:(id)sender {

	HomeScreenViewController *homeScreenVC=[self.storyboard instantiateViewControllerWithIdentifier:@"HomeScreenViewController"];
    homeScreenVC.isEnable=NO;
    homeScreenVC.isDisable=YES;
	homeScreenVC.showHideSelection=@"2";
	[self.navigationController pushViewController:homeScreenVC animated:YES];
}
@end
