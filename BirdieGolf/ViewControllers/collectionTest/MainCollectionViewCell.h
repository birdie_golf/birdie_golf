//
//  MainCollectionViewCell.h
//  BirdieGolf
//
//  Created by Arpit Shukla on 25/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMGolfCourseNameModel.h"
#import "YTPlayerView.h"

@interface MainCollectionViewCell : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UILabel *labelMain;
@property (strong, nonatomic) IBOutlet UIImageView *holeScoreCardImgV;
@property (strong, nonatomic) IBOutlet UIButton *viewScoreCardBtn;
@property (strong, nonatomic) IBOutlet YTPlayerView *playerView;


- (void)updateCell:(id)inStrLabel;
- (void)updateCellHoleNumberLabel:(int)inNumber;
@end
