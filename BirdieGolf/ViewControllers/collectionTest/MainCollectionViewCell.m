
//  MainCollectionViewCell.m
//  BirdieGolf
//
//  Created by Arpit Shukla on 25/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import "MainCollectionViewCell.h"

@implementation MainCollectionViewCell

- (void)updateCell:(id)inStrLabel
{
//    [self.playerView loadWithVideoId:inStrLabel];
	NSDictionary *playerVars = @{
								 @"controls" : @"1",
								 @"playsinline" : @"1",
								 @"autohide" : @"1",
								 @"showinfo" : @"1",
								 @"autoplay" : @"0",
								 @"fs" : @"1",
								 @"rel" : @"0",
								 @"loop" : @"0",
								 @"enablejsapi" : @"1",
								 @"modestbranding" : @"1"
								 };
	[self.playerView loadWithVideoId:inStrLabel playerVars:playerVars];
	
}
- (void)updateCellHoleNumberLabel:(int)inNumber
{
    _labelMain.text=[NSString stringWithFormat:@"Hole %d",inNumber];
    _labelMain.font=[UIFont fontWithName:@"Arial Rounded MT Bold" size:10.0];
}
@end
