//
//  MainCollectionViewController.h
//  BirdieGolf
//
//  Created by Arpit Shukla on 25/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CMGolfCourseNameModel.h"
#import "MainCollectionViewCell.h"
#import "StaticMapViewController.h"
#import "CMGolfCourseNameModel.h"
@interface MainCollectionViewController : UIViewController 
@property (strong, nonatomic) IBOutlet      UICollectionView            *           mainCollectionVC;

@property (nonatomic, strong)CMGolfCourseNameModel *aGolfCourseName;
- (IBAction)viewScoreCardBtn:(id)sender;
- (NSMutableArray *)holeVideoData:(NSMutableArray*)inHoleVideoUrl;
@end
