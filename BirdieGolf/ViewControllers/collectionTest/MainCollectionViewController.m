//
//  MainCollectionViewController.m
//  BirdieGolf
//
//  Created by Arpit Shukla on 25/02/16.
//  Copyright © 2016 AppRoutes. All rights reserved.
//
#import "MainCollectionViewCell.h"
#import "MainCollectionViewController.h"
#import "CMGolfCourseNameModel.h"
#import "WelcomeScreenViewController.h"
#import "ScoreCardViewController.h"
#import "UrlsManager.h"

@interface MainCollectionViewController ()
{
    
    CMGolfCourseNameModel *golfCourse;
    NSMutableArray *golfList;
    MainCollectionViewCell *MainCell;
    UIButton *someButton ;
	
//    WelcomeScreenViewController *wlcmScreenVC;
}

@end

@implementation MainCollectionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
   //    [self.navigationController pushViewController:scoreCardVC animated:YES];
    // Do any additional setup after loading the view.
//    self.navigationItem.hidesBackButton=YES;
    [self customNavigationItems];
//     wlcmScreenVC=[self.storyboard instantiateViewControllerWithIdentifier:@"WelcomeScreenViewController"];
//    [self customBackBtn];
}
- (void)viewWillAppear:(BOOL)animated
{
    [_mainCollectionVC reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)viewWillDisappear:(BOOL)animated
{
	 [_mainCollectionVC reloadData];
}
- (NSMutableArray *)holeVideoData:(NSMutableArray*)inHoleVideoUrl
{
    return inHoleVideoUrl;
}
- (void)customNavigationItems
{
    self.navigationItem.title=@"Royal Sydney Golf Club";
    UIImage* image3 = [UIImage imageNamed:@"staticMap22.png"];
    CGRect frameimg = CGRectMake(11,11, image3.size.width, image3.size.height);
    someButton = [[UIButton alloc] initWithFrame:frameimg];
    [someButton setBackgroundImage:image3 forState:UIControlStateNormal];
    [someButton addTarget:self action:@selector(staticMapView)
         forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *listButton =[[UIBarButtonItem alloc]initWithCustomView:someButton];
    self.navigationItem.rightBarButtonItem=listButton;
    self.navigationItem.hidesBackButton=NO;
	[self customLeftBarBtnImage];
}
- (void)customLeftBarBtnImage
{
    UIButton *backBtn =[UIButton buttonWithType:UIButtonTypeCustom];
	UIImage *imageBackBtn=[UIImage imageNamed:@"backIconWhite"];
	[backBtn setFrame:CGRectMake(0.0f,0.0f,imageBackBtn.size.width, imageBackBtn.size.height)];
	[backBtn setBackgroundImage:imageBackBtn forState:UIControlStateNormal];
    [self.navigationItem setLeftBarButtonItem:[[UIBarButtonItem alloc] initWithCustomView:backBtn]];
	[backBtn addTarget:self action:@selector(popToViewController) forControlEvents:UIControlEventTouchUpInside];
}
- (void)popToViewController
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_aGolfCourseName.holeVideoUrl count];
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	NSString *SimpleIdentifier = @"collectionCell";
		MainCollectionViewCell  *cell=[collectionView dequeueReusableCellWithReuseIdentifier:SimpleIdentifier forIndexPath:indexPath];
		cell.layer.cornerRadius=6.0f;
		cell.layer.borderWidth=1.0f;
		cell.layer.borderColor=[UIColor whiteColor].CGColor;
		[cell updateCell:[_aGolfCourseName.holeVideoUrl objectAtIndex:indexPath.row]];
		[cell updateCellHoleNumberLabel:(indexPath.row+1.0)];
	
    return cell;
}


- (void)staticMapView
{
    StaticMapViewController *StaticView=[self.storyboard instantiateViewControllerWithIdentifier:@"staticMap"];
  //  StaticView.delegate = self;
    [self.navigationController pushViewController:StaticView animated:YES];
}

- (IBAction)viewScoreCardBtn:(id)sender
{
	 ScoreCardViewController *aScoreCardVC=[self.storyboard instantiateViewControllerWithIdentifier:@"ScoreCardViewController"];
	aScoreCardVC.noOfHoleStr=_aGolfCourseName.numOfHole;
//	aScoreCardVC.aCMGolfCourseNameModel=_aGolfCourseName;
    [self.navigationController pushViewController:aScoreCardVC animated:YES];
}

@end
